package com.ideas2it.controller;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.ProjectController;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Employee;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for Employee Details which interacts with the user.
 * The user can add, create, update and delete the detail of an employee.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
class EmployeeController {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private EmployeeView employeeView = new EmployeeView();

    /**
     * <p>
     * Displaying employee's in a project by the project id given by user.
     * </p>
     * 
     * @param projectService - which has the details of projects.
     */
    public void displayEmployeesByProjectId(String projectId) {
        if (null == projectId) {
            employeeView.showMessage(String.format(Constants.MSG_NOT_EXIST, 
                                         Constants.PROJECT_NAME, projectId));
        } else {
            try {
               for (Employee employee : employeeService
                                          .getEmployeesByProjectId(projectId))
                employeeView.displayEmployee(employee);
            } catch(ApplicationException e) {
                ApplicationLogger.error(Constants.EMP_RD_ALL_PROJ_ERR
                                           + projectId, e);
                employeeView.showMessage(Constants.USR_ERR_UNABLE);
            }
        }
    }

    /**
     * <p>
     * Adding a new employee to employees.
     * </p>
     */
    public void addNewEmployee() {
        Employee employee = employeeView.getEmployeeDetailsFromUser();
        boolean notInserted = false;
        do {
            try {
                employeeService.addEmployee(employee);
                notInserted = false;
            } catch(UserInputException e){
                notInserted = true;
                if (((Employee)e.getObject()).getProject() == null) {
                   employeeView.showMessage(Constants.PROJECT_ID + Constants
                                               .MSG_NOT_EXIST);
                   employeeView.showMessage(employeeService.getProjectNames());
                }
                employeeView.showMessage((Constants.USR_ERR_INVALID_INPUT));
                employee = employeeView.getValidEmployeeDetailsFromUser
                               ((Employee)e.getObject());
            } catch(ApplicationException e){
                ApplicationLogger.error(Constants.EMP_INS_ERR
                                           + employee.getId(), e);
                employeeView.showMessage(Constants.EMPLOYEE +
                                             Constants.MSG_INS_UNSUCC);
                return;
            } 
        } while(notInserted);
        employeeView.showMessage(String.format(Constants.MSG_SUCCESS, 
                                     Constants.EMP_ID));
    }


    public void displayAllEmployees() {
        try {
            employeeView.showMessage(employeeService.getAllEmployees());
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ALL_PROJ_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Deleting an employee from employees by the id given by user.
     * </p>
     */
    public void deleteAnEmployee() {
        String id = UI.getUserInput(Constants.EMPLOYEE_ID);
        if (UI.getConfirmationFromUser()) {
            try {
                if (employeeService.deleteEmployeeById(id)) {
                    employeeView.showMessage(String.format(Constants.
                                         MSG_DELETION,Constants.EMPLOYEE, id));
                } else {
                    employeeView.showMessage(String.format(Constants.
                                         MSG_NOT_EXIST,Constants.EMPLOYEE, id));
                }
            } catch(ApplicationException e){
                ApplicationLogger.error(Constants.EMP_DEL_ERR + id, e);
                employeeView.showMessage(Constants.USR_ERR_UNABLE);
            }
        } 
    }

    /**
     * <p>
     * Updating an employee's attribute by employee id(given by user).
     * </p>
     */
    public void updateEmployeeDetails() {
        String employeeId = UI.getUserInput(Constants.EMPLOYEE_ID);
        String attribute = null;
        String status = null;
        int choice;
        do {
            employeeView.showEmployeeUpdateMenu();
            choice = Integer.parseInt(UI.getUserInput("Choice"));
            switch(choice) {
                case 1:
                    attribute = Constants.NAME;
                    break;
                case 2:
                    attribute = Constants.EMAIL;
                    break;
                case 3:
                    attribute = Constants.DOB;
                    break;
                case 4:
                    attribute = Constants.SALARY;
                    break;
                case 0:
                    return;
                default:
                    employeeView.showMessage(Constants.OPTION_INVALID_MSG);
            }
            try {
            status = employeeService.updateEmployee
                         (employeeId, attribute, UI.getUserInput(attribute));
            } catch(ApplicationException e) {
                ApplicationLogger.error(String.format(Constants.EMP_UPD_ERR,
                                           employeeId, attribute, null), e);
                employeeView.showMessage(Constants.MSG_UPD_UNSUCC);
            } catch(UserInputException e) {
                ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
                employeeView.showMessage(e.getErrorCode()+Constants.MSG_NOT_EXIST);
            }
        } while(0 != choice);
    }

    /**
     * <p>
     * Assigning a project to an employee.
     * </p>
     *
     * @param id - is the employee whoose attribute is updated.
     * @param attribute - which is updated in employee.
     * @param  status - indicates whether the attribute is updated or not.
     */
    public void assignProjectToEmployee() {
        String status = " ";
        boolean notUpdated = false;
        UI.display("List of Employees ::");
        displayAllEmployees();
        String employeeId = UI.getUserInput(Constants.EMPLOYEE_ID);
        UI.display("List of Projects ::");
        employeeView.showMessage(employeeService.getProjectNames());
        try {
            status = employeeService.updateEmployee(employeeId, Constants.
                            PROJECT_ID, UI.getUserInput(Constants.
                            PROJECT_ID));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        } catch(UserInputException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            employeeView.showMessage(e.getErrorCode()+Constants.MSG_NOT_EXIST);
        }
        if (status.equals(Constants.STATUS_OK)) {
            employeeView.showMessage(Constants.MSG_PROCESS_DONE);
        }
    }

    /**
     * <p>
     * Removing project from an employee.
     * </p>
     *
     * @param projectService - specifies the employee for which project is 
     *                         unassigned.
     */
    public void unassignProjectFromEmployee() {
        UI.display("List of Employees ::");
        displayAllEmployees();
        String id = UI.getUserInput(Constants.EMPLOYEE_ID);
            try {
                if (UI.getConfirmationFromUser()) {
                    String status = employeeService.updateEmployee(id,
                                     Constants.PROJECT_ID, Constants.ON_BENCH);
                    employeeView.showMessage(Constants.MSG_PROCESS_DONE);
                }
            } catch(ApplicationException e) {
                ApplicationLogger.error(Constants.EMP_UNASSIGN_PROJ_ERR, e);
                employeeView.showMessage(Constants.USR_ERR_UNABLE);
            } catch(UserInputException e) {
                ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
                employeeView.showMessage(e.getErrorCode() +
                                            Constants.MSG_NOT_EXIST);
            }
    }

    public void displayEmployee() {
        try {
            employeeView.displayEmployee
                        (employeeService.getEmployeeById(UI.
                            getUserInput(Constants.EMP_ID)));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        }
    } 

    /**
     * <p>
     * Controlling the CRUD operation on employee details based on the choice 
     * given by the user.
     * </p>
     */
    public void doEmployeeCRUD() {
        int choice = 0;
        do {
            employeeView.showEmployeeCRUDMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput("Choice"));
            } catch(NumberFormatException e) {
                choice = 99;
            }
            switch(choice) {
                case 1:
                    addNewEmployee();
                    break;
                case 2:
                    displayEmployee();
                    break;
                case 3:
                    updateEmployeeDetails();
                    break;
                case 4:
                    deleteAnEmployee();
                    break;
                case 5:
                    assignProjectToEmployee();
                    break;
                case 6:
                    unassignProjectFromEmployee();
                    break;
                case 7:
                    displayEmployeesByProjectId(employeeService.
                        getProjectIdByName(UI.getUserInput
                             (Constants.PROJECT_NAME)));
                    break;
                case 0:
                    return;
                default:
                    employeeView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }

    /**
     * <p>
     * Controlling the operation on employee and project details based on the 
     * choice of user.
     * </p>
     */
    public static void main(String args[]) { 
        int choice;
        EmployeeController employeeController = new EmployeeController();
        ProjectController projectController = new ProjectController();
        ClientController clientContoller = new ClientController();
        AddressController addressController = new AddressController();
        do {
            EmployeeView.showMainMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput("Choice"));
            } catch(NumberFormatException e) {
                choice = 99;
            }
            switch(choice) {
                case 1:
                    employeeController.doEmployeeCRUD();
                    break;
                case 2:
                    projectController.doProjectCRUD();
                    break;
                case 3:
                    clientContoller.doClientCRUD();
                    break;
                case 4:
                    addressController.doAddressCRUD();
                    break;
                case 0:
                    System.exit(0);
                default:
                    UI.display(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }
}
