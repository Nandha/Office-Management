package com.ideas2it.controller;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.ProjectController;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Address;
import com.ideas2it.service.AddressService;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.ClientView;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.view.AddressView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for Address Details which interacts with the user.
 * The user can add, create, update and delete the detail of an address.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
class AddressController {
    private AddressService addressService = new AddressServiceImpl();
    private AddressView addressView = new AddressView();

    /**
     * <p>
     * Adding a new address to addresss.
     * </p>
     */
    public void addNewAddress(String holderId) {
        Address address = addressView.getAddressDetailsFromUser();
        boolean notInserted = false;
        do {
            try {
                address.setHolderId(holderId);
                System.out.println(address.getHolderId()+holderId);
                addressService.addAddress(address);
                notInserted = false;
            } catch(UserInputException e){
                notInserted = true;
                addressView.showMessage((Constants.USR_ERR_INVALID_INPUT));
                address = addressView.getValidAddressDetailsFromUser
                               ((Address)e.getObject());
            } catch(ApplicationException e){
                ApplicationLogger.error(Constants.EMP_INS_ERR
                                           + address.getHolderId(), e);
                addressView.showMessage(Constants.EMPLOYEE +
                                             Constants.MSG_INS_UNSUCC);
                return;
            } 
        } while(notInserted);
        addressView.showMessage(String.format(Constants.MSG_SUCCESS, 
                                     Constants.EMP_ID));
    }

    /**
     * <p>
     * Deleting an address from addresss by the id given by user.
     * </p>
     */
    public void deleteAnAddress(String holderId) {
        if (UI.getConfirmationFromUser()) {
            try {
                if (addressService.deleteAddressByHolderId(holderId)) {
                    addressView.showMessage(String.format(Constants.
                                  MSG_DELETION,Constants.EMPLOYEE, holderId));
                } else {
                    addressView.showMessage(String.format(Constants.
                                  MSG_NOT_EXIST,Constants.EMPLOYEE, holderId));
                }
            } catch(ApplicationException e){
                ApplicationLogger.error(Constants.EMP_DEL_ERR + holderId, e);
                addressView.showMessage(Constants.USR_ERR_UNABLE);
            }
        } 
    }

    /**
     * <p>
     * Updating an address's attribute by address id(given by user).
     * </p>
     */
    public void updateAddressDetails(String holderId) {
        String attribute = null;
        String status = null;
        int choice;
        do {
            addressView.showAddressUpdateMenu();
            choice = Integer.parseInt(UI.getUserInput("Choice"));
            System.out.println(choice);
            switch(choice) {
                case 1:
                    attribute = Constants.HOLDER_NAME;
                    break;
                case 2:
                    attribute = Constants.ADDRESS_ONE;
                    break;
                case 3:
                    attribute = Constants.ADDRESS_TWO;
                    break;
                case 4:
                    attribute = Constants.CITY;
                    break;
                case 5:
                    attribute = Constants.ZIP;
                    break;
                case 6:
                    attribute = Constants.STATE;
                    break;
                case 7:
                    attribute = Constants.COUNTRY;
                    break;
                case 8:
                    attribute = Constants.LANDMARK;
                    break;
                case 0:
                    return;
                default:
                    addressView.showMessage(Constants.OPTION_INVALID_MSG);
            }
            try {
            status = addressService.updateAddress
                         (holderId, attribute, UI.getUserInput(attribute));
            } catch(ApplicationException e) {
                ApplicationLogger.error(String.format(Constants.EMP_UPD_ERR,
                                           holderId, attribute, null), e);
                addressView.showMessage(Constants.MSG_UPD_UNSUCC);
            }/* catch(UserInputException e) {
                ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
                addressView.showMessage(e.getErrorCode()+Constants.MSG_NOT_EXIST);
            }*/
        } while(0 != choice);
    }


    public void displayAddress(String holderId) {
        try {
            addressView.displayAddress
                        (addressService.getAddressesByHolderId(holderId));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            addressView.showMessage(Constants.USR_ERR_UNABLE);
        }
    } 

    public void displayAllClients() {
        ClientView clientView = new ClientView();
        try {
            clientView.displayClient
                        (addressService.getAllClients());
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            addressView.showMessage(Constants.USR_ERR_UNABLE);
        }
    } 

    public void displayAllEmployees() {
        EmployeeView employeeView = new EmployeeView();
        try {
            employeeView.showMessage
                        (addressService.getAllEmployees());
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            addressView.showMessage(Constants.USR_ERR_UNABLE);
        }
    } 

    /**
     * <p>
     * Controlling the CRUD operation on address details based on the choice 
     * given by the user.
     * </p>
     */
    public void doAddressCRUD() {
        int choice = 0;
        addressView.showMessage("List of Employees::");
        displayAllEmployees();
        displayAllClients();
        String holderId = UI.getUserInput(Constants.HOLDER_ID);
        do {
            addressView.showAddressCRUDMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput("Choice"));
            } catch(NumberFormatException e) {
                choice = 99;
            }
            switch(choice) {
                case 1:
                    addNewAddress(holderId);
                    break;
                case 2:
                    displayAddress(holderId);
                    break;
                case 3:
                    updateAddressDetails(holderId);
                    break;
                case 4:
                    deleteAnAddress(holderId);
                    break;
                case 0:
                    return;
                default:
                    addressView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }

    /**
     * <p>
     * Controlling the operation on address and project details based on the 
     * choice of user.
     * </p>
     */
    public static void main(String args[]) { 
        new AddressController().doAddressCRUD();
    }
}
