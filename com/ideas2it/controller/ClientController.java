package com.ideas2it.controller;

import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Client;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.service.ClientService;
import com.ideas2it.view.ClientView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for Client Details which interacts with the user.
 * The user can add, create, update and delete the detail of an Client.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
class ClientController {

    public ClientService clientService = new ClientServiceImpl();
    private ClientView clientView = new ClientView();

    /**
     * <p>
     * Adding a new Client to Clients.
     * </p>
     */
    public void addClient() {
        Client client = clientView.getClientDetailsFromUser();
        String status = null;
        try {
            status = clientService.addClient(client);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_INS_ERR
                                       + client.getId(), e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
            return;
        }
        if (!status.equals(Constants.STATUS_OK)) {
            clientView.showMessage(String.format(Constants.MSG_CHECK_FORMAT,
                                         status));
            clientView.showCorrectFormat(status);
        }
    }

    /**
     * <p>
     * Deleting an Client from Clients by the id given by user.
     * </p>
     */
    public void deleteClient() {
        String id = UI.getUserInput(Constants.CLIENT_ID);
        boolean deleted = false;
        try {
            if (UI.getConfirmationFromUser() && clientService.
                   deleteClientById(id))
                deleted = true;
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_DEL_ERR + id, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
        if (deleted) {
            clientView.showMessage(String.format(Constants.MSG_DELETION,
                                         Constants.CLIENT, id));
        } else {
            clientView.showMessage(String.format(Constants.MSG_NOT_EXIST,
                                               Constants.CLIENT, id));
        }
    }

    /**
     * <p>
     * Updating an Client's attribute by Client id(given by user).
     * </p>
     */
    public void updateClientDetails() {
        String id = UI.getUserInput(Constants.CLIENT_ID);
        String attribute = null;
        String status = null;
        int choice = 0;
        do {
            clientView.showClientUpdateMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput("Choice"));
                switch(choice) {
                    case 1:
                        attribute = Constants.NAME;
                        status = clientService.updateClient(id, attribute,
                                    UI.getUserInput(attribute));
                        break;
                    case 2:
                        attribute = Constants.EMAIL;
                        status = clientService.updateClient(id, attribute,
                                    UI.getUserInput(attribute));
                        break;
                    case 0:
                        return;
                    default:
                        clientView.showMessage(Constants.OPTION_INVALID_MSG);
                }
            } catch(NumberFormatException e) {
                clientView.showMessage(Constants.OPTION_INVALID_MSG);
            } catch(ApplicationException e) {
                ApplicationLogger.error(Constants.PROJ_UPD_ERR + id, e);
                clientView.showMessage(Constants.USR_ERR_UNABLE);
            }
           // userInterface.displayUpdationStatus(id, attribute, status);
        } while(0 != choice);
    }

    public String getIdByClientName(String ClientName) {
        try {
            return clientService.getIdByClientName(ClientName);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ID_ERR + ClientName, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
        return null;
    }

    public void displayAllClients() {
        try {
            for(Client client : clientService.getAllClients())
                clientView.displayClient(client);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ALL_ERR, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    public void displayClientById() {
        try {
            clientView.displayClient
                        (clientService.getClient(UI.
                                       getUserInput(Constants.CLIENT_ID)));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ERR, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }


    /**
     * <p>
     * Controlling the CRUD operation on Client details based on the choice 
     * of user.
     * </p>
     */
    public void doClientCRUD() { 
        int choice;
        do {
            clientView.showClientCRUDMenu();
            choice = Integer.parseInt(UI.getUserInput("Choice"));

            switch(choice) {
                case 1:
                    addClient();
                    break;
                case 2:
                    displayClientById();
                    break;
                case 3:
                    updateClientDetails();
                    break;
                case 4:
                    deleteClient();
                    break;
                case 0:
                    return;
                default:
                    clientView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }

    public static void main(String args[]) {
        new ClientController().doClientCRUD();
    }
}
