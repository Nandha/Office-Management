package com.ideas2it.controller;

import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Project;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.view.ProjectView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for project Details which interacts with the user.
 * The user can add, create, update and delete the detail of an project.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
class ProjectController {

    public ProjectService projectService = new ProjectServiceImpl();
    private ProjectView projectView = new ProjectView();

    /**
     * <p>
     * Adding a new project to projects.
     * </p>
     */
    public void addProject() {
        Project project = projectView.getProjectDetailsFromUser();
        String status = null;
        try {
            status = projectService.addProject(project);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_INS_ERR
                                       + project.getId(), e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
            return;
        }
        if (!status.equals(Constants.STATUS_OK)) {
            projectView.showMessage(String.format(Constants.MSG_CHECK_FORMAT,
                                         status));
            projectView.showCorrectFormat(status);
        }
    }

    /**
     * <p>
     * Deleting an project from projects by the id given by user.
     * </p>
     */
    public void deleteProject() {
        String id = UI.getUserInput(Constants.PROJECT_ID);
        boolean deleted = false;
        try {
            if (UI.getConfirmationFromUser() && projectService.
                   deleteProjectById(id))
                deleted = true;
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_DEL_ERR + id, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
        if (deleted) {
            projectView.showMessage(String.format(Constants.MSG_DELETION,
                                         Constants.PROJECT, id));
        } else {
            projectView.showMessage(String.format(Constants.MSG_NOT_EXIST,
                                               Constants.PROJECT, id));
        }
    }

    /**
     * <p>
     * Updating an project's attribute by project id(given by user).
     * </p>
     */
    public void updateProjectDetails() {
        String id = UI.getUserInput(Constants.PROJECT_ID);
        String attribute = null;
        String status = null;
        int choice = 0;
        do {
            projectView.showProjectUpdateMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput("Choice"));
                switch(choice) {
                    case 1:
                        attribute = Constants.NAME;
                        status = projectService.updateProject(id, attribute,
                                    UI.getUserInput(attribute));
                        break;
                    case 0:
                        return;
                    default:
                        projectView.showMessage(Constants.OPTION_INVALID_MSG);
                }
            } catch(NumberFormatException e) {
                projectView.showMessage(Constants.OPTION_INVALID_MSG);
            } catch(ApplicationException e) {
                ApplicationLogger.error(Constants.PROJ_UPD_ERR + id, e);
                projectView.showMessage(Constants.USR_ERR_UNABLE);
            }
           // userInterface.displayUpdationStatus(id, attribute, status);
        } while(0 != choice);
    }

    /**
     * <p>
     * Getting a project id by the given project name.
     *
     * @param projectName by which project id is read.
     *
     * @return project id which got by the project name.
     * </p>
     */
    public String getIdByProjectName(String projectName) {
        try {
            return projectService.getIdByProjectName(projectName);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ID_ERR + projectName, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
        return null;
    }

    /**
     * <p>
     * Displaying all the existing projects.
     * </p>
     */
    public void displayAllProjects() {
        try {
            for(Project project : projectService.getAllProjects())
                projectView.displayProject(project);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ALL_ERR, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Assigning a employee to an project.
     * </p>
     *
     * @param id - is the employee whoose attribute is updated.
     * @param attribute - which is updated in employee.
     * @param  status - indicates whether the attribute is updated or not.
     */
    public void assignEmployeesToProject() {
        UI.display("List of Projects ::");
        displayAllProjects();
        String projectId = UI.getUserInput(Constants.PROJECT_ID);
        UI.display("List of Employees ::");
        projectView.showMessage(projectService.getAllEmployees());
        try {
             projectService.assignEmployeesToProject(UI.getUserInput
                       (Constants.EMPLOYEE_ID),  projectId);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        } catch(UserInputException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            projectView.showMessage(e.getErrorCode()+Constants.MSG_NOT_EXIST);
            assignEmployeesToProject();
            return;
        }
    }

    /**
     * <p>
     * Displaying a project by the id given by user..
     * </p>
     */
    public void displayProjectById() {
        try {
            projectView.displayProject
                        (projectService.getProject(UI.
                                       getUserInput(Constants.PROJECT_ID)));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ERR, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    public void doProjectOperation() {
        int count = 0;
        displayAllProjects();
        String projectId = getIdByProjectName(UI.
                                         getUserInput(Constants.PROJECT_NAME));
        //TODO assign a set of employees to a project.
        //while(count < userInterface.getUserInput
        //                      ("NO OF EMPLOYEES YOU WANT TO ADD"));
    }


    /**
     * <p>
     * Controlling the CRUD operation on project details based on the choice 
     * of user.
     * </p>
     */
    public void doProjectCRUD() { 
        int choice;
        do {
            projectView.showProjectCRUDMenu();
            choice = Integer.parseInt(UI.getUserInput("Choice"));

            switch(choice) {
                case 1:
                    addProject();
                    break;
                case 2:
                    displayProjectById();
                    break;
                case 3:
                    updateProjectDetails();
                    break;
                case 4:
                    deleteProject();
                case 5:
                    assignEmployeesToProject();
                    break;
                case 0:
                    return;
                default:
                    projectView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }
}
