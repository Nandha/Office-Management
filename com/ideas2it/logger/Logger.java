package com.ideas2it.logger;

import org.apache.log4j.Logger;

import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Implementing custom exception by providing meaningfull information when an 
 * abnormal problem occurs due to User input.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 29/07/2017.
 */
public class ApplicationLogger {

   static Logger log = Logger.getLogger("Application Logger");

   public static void main(String[] args) {
       log.error("ayyayoo......");
   }
}
