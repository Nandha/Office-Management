package com.ideas2it.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import com.ideas2it.common.Constants;
import com.ideas2it.logger.ApplicationLogger;

/**
 * <p>
 * validating the data which contains a meaningful information given by a user.
 * The data should be in a specified format so that it would be meaningful.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017. 
 */
public class Validation {
 
    /**
     * <p>
     * validating name whether it is in the specified format.
     * </p>
     *
     * @param name - is to be validated as it should have only alphabet
     *               and space. 
     *               EX:- correct format - virat kohli
     *               EX:- incorrect format - @virat kohli94  
     *
     * @return true if name is in specified format else return false.
     */
    public static boolean validateName(String name) {   
        return (name.matches(Constants.NAME_FORMAT));
    }

    /**
     * <p>
     * validating Email id whether it is in the specified format.
     * </p>
     *
     * @param emailId - is to be validated as it should be in the following
     *                  format. 
     *                  Ex:- correct format - kohli1994@gmail.com
     *                  Ex:- incorrect format - #kohli1994@gmail,co
     *
     * @return true if emailId is in specified format else return false.
     */
    public static boolean validateEmail(String emailId) { 
        emailId = emailId.toLowerCase();  
        return (emailId.matches(Constants.EMAIL_FORMAT) && 
                   (emailId.indexOf(Constants.AT) == emailId.lastIndexOf
                   (Constants.AT)) && (emailId.endsWith(Constants.DOMAIN_COM) ||
                       emailId.endsWith(Constants.DOMAIN_NET)));
    }

    /**
     * <p>
     * validating Salary of the employee.
     * </p>
     *
     * @param Salary - is to be validated as it should be a numeric and greater
     *                 than 4000.
     *
     * @return true if Salary is in specified format else return false.
     */
    public static boolean validateSalary(String salary) {   
        return (salary.matches(Constants.REGEX_NUM) && 
                   (Integer.parseInt(salary) >= Constants.MIN_SALARY));
    }

    /**
     * <p>
     * checking date whether it is in the format of YYYY-MM-DD.
     * </p>
     *
     * @param date - is to be checked as it should contain only numeric and 
     *               hyphen as follows.
     *               EX:- correct format - 1994-06-21
     *               EX:- incorrect formats - 21-06-1994 and 1994-Jun-21
     *
     * @return true if date satisfies the format else return false.
     */
    public static boolean checkDateFormat(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(Constants.
                                                               DATE_FORMAT);
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(format.parse(date));
            return checkDateValues(calendar.get(Calendar.YEAR), calendar
                  .get(Calendar.MONTH), Integer.parseInt(date.substring(8,10)));
        } catch (ParseException e) {
            ApplicationLogger.error(Constants.INVALID + date
                                       + Constants.MSG_NAME_FORMAT, e);
            return false;
        }

    }

    /**
     * <p>
     * checking the values of date in general manner. EX:- days shoud not be 
     * greater than 31 and less than 1, month should be within the range of 
     * 1 and 12.
     * </p>
     *
     * @param year - is to be checked as it is a valid one. ie)1000 <year< 10000
     * @param month - is to be checked as it is a valid one. ie) 1 >= month<= 12
     * @param days - is to be checked as it is a valid one. ie) 1 >= days <= 31
     *
     * @return true if year, month and days are all valid else return false
     *              (even anyone of them are not valid return false).
     */
    public static boolean checkDateValues(int year, int month, int days) {
        try {
            Calendar calender = new GregorianCalendar();
            calender.setLenient(false);
            calender.set(year, month, days);
            calender.getTime();
            return isBeforeToday(new Date(year, month, days));
        } catch (IllegalArgumentException e) {
            ApplicationLogger.error(Constants.INVALID + new Date(year, month,
                                                                    days), e);
            return false;
        }
    }

    /**
     * <p>
     * checking date whether it comes before the date of today.
     * </p>
     *
     * @param date is to be checked as it should be before today's date.
     * EX:-if date=1994-06-21 and today=2017-07-18 then return true.
     *     if date=2017-07-21 and today=2017-07-18 then return false.
     *
     * @return true if date is before today's date else return false.
     */
    public static boolean isBeforeToday(Date date) {
        Date today = new Date(); // which assigns today's date.
        return today.before(date);
    }

    /**
     * <p>
     * checking data whether it has only alphabet and number.
     * </p>
     *
     * @param data is to be checked as it should have only alphabet and number.
     * EX:-if data="Flat 3" is then return true.
     *     if data="Flat@#% 8" is then return false.
     *
     * @return true if date is before today's date else return false.
     */
    public static boolean isValidAddress(String data) {   
        return (data.matches(Constants.ADDR_FORMAT));
    }

    public static boolean isNumber(String number) {
        return (number.matches(Constants.NUM_FORMAT));
    }
}
