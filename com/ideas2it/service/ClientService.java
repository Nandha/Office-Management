package com.ideas2it.service;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Client;
import java.util.List;

/**
 * <p>
 * Implementing a Service for CRUD operation on Client details. The buisness 
 * logics such as adding a Client to Clients, deleting a Client from 
 * Clients and validating the information of an Client(such as name) are 
 * implemented.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public interface ClientService {

    /** 
     * <p>
     * Adding a client to clients.
     * </p>
     * 
     * @param client - which is to be added in clients.
     *
     * @return status which indicates the contoller whether client is added or
     *                the details are not in the specified format.
     *
     * @exception ApplicationException - if SQL error occurs, when inserting a
     *            client detail.
     */
    public String addClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Reading an client from client's by the given client Id.
     * </p>
     * 
     * @param id - which specifies the client to be searched in clients.
     * 
     * @return client for which client id matching with the given id.
     *
     * @exception ApplicationException - if SQL error occurs, when reading an
     *            client by the given client id.
     */
    public Client getClient(String id) throws ApplicationException;

    /**
     * <p>
     * Reading all the clients.
     * </p>
     * 
     * @param clientId - which is to be matched with client's client id.
     * 
     * @return clients for which the client Id is matching with the given
     *                   client id.
     *
     * @exception ApplicationException - if SQL error occurs when reading all the
     *            clients.
     */
    public List<Client> getAllClients() throws ApplicationException;

    /**
     * <p>
     * Validating & updating an attribute of the given client by the given data
     * </p>
     * 
     * @param attribute - is to be updated in an client specified by the id.
     * @param data - which is to be replaced in the specified attribute of an 
     *               client(specified by id).
     * 
     * @return status whether the given attribute is updated by the given data
     *              else return the attribute as it not satisfying the format.
     *
     * @exception ApplicationException - if SQL error occurs when updating an
     *            client by the given data.
     */
    public String updateClient(String clientId, String attribute,
                                   String data) throws ApplicationException;

    /**
     * <p>
     * Deleting a client from clients by the given id.
     * </p>
     * 
     * @param id - which specifies the client to be deleted from clients.
     * 
     * @return true if the client specified the given id is deleted else
     *              return false.
     *
     * @exception ApplicationException - if SQL error occurs when deleting an
     *            client by the given client id.
     */
    public boolean deleteClientById(String id) throws ApplicationException;

    /**
     * <p>
     * Reading a client Id by the given client name from client's.
     * </p>
     * 
     * @param name - which is to be searched in clients.
     * 
     * @return client id for which client name is matching with the given
     *                    name.
     *
     * @exception ApplicationException - if SQL error occurs when reading an
     *            client id by the given client name.
     */
    public String getIdByClientName(String name) throws ApplicationException;

    /**
     * <p>
     * validating client's details such as name.
     * </p>
     *
     * @param client - which contains the details that has to be
     *                        validated.
     *
     * @return status which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     */
    public String validateDetails(Client client);

    /**
     * <p>
     * validating an specified attribute by the given data.
     * </p>
     *
     * @param attribute - which specifies the type of data.
     *
     * @param data - which has to be validated based on attribute type.
     * 
     * @return true if data is in specified format else return false.
     */
    public boolean validateAttribute(String attribute, String data);
 }



