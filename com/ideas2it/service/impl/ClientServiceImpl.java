package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.ClientDAO;
import com.ideas2it.dao.impl.ClientDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.service.ClientService;
import com.ideas2it.model.Client;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on client details. The buisness 
 * logics such as adding a client to clients, deleting a client from 
 * clients and validating the information of an client(such as name) are 
 * implemented.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public class ClientServiceImpl implements ClientService {
    private ClientDAO clientDao = new ClientDAOImpl();

    /**
     * @see com.ideas2it.service.clientService#addclient(client).
     */
    public String addClient(Client client) throws ApplicationException {
        if(null == client){
            return " ";
        } else {
            String status = validateDetails(client);
            if (status.equals(Constants.STATUS_OK)) {
                        clientDao.insertClient(client);
                    return Constants.STATUS_OK;
            } else {
                return status;
            }
        }
    }

    /**
     * @see com.ideas2it.service.clientService#getclient(String).
     */
    public Client getClient(String id) throws ApplicationException {
		return clientDao.readClientById(id);
    }

    /**
     * @see com.ideas2it.service.clientService#getAllclients().
     */
    public List<Client> getAllClients() throws ApplicationException {
       return clientDao.readAllClient();
    }

    /**
     * @see com.ideas2it.service.clientService#updateclient(String, String,
     *          String).
     */
    public String updateClient(String clientId, String attribute, String data
                                   ) throws ApplicationException {
        if (validateAttribute(attribute, data)) {
            attribute = getColumnName(attribute);
            if (clientDao.updateClient(clientId, attribute, data) > 0) {
                return Constants.STATUS_OK;
            }
        } else {
            return attribute;
        }
        return null;
    }

    /**
     * @see com.ideas2it.service.clientService#deleteclientById(String).
     */
    public boolean deleteClientById(String id) throws ApplicationException {
        return (clientDao.deleteClientById(id) > 0);
    }

    /**
     * @see com.ideas2it.service.clientService#getIdByclientName(String).
     */
    public String getIdByClientName(String name) throws ApplicationException {
        return clientDao.readClientIdByName(name);
    }

    public String getColumnName(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            return Constants.ATTRIB_CLIENT_NAME;
        } if (attribute.equals(Constants.EMAIL)) {
            return Constants.ATTRIB_CLIENT_EMAIL;
        } else {
            return " ";
        }
    }

    /**
     * <p>
     * validating client's details such as name.
     * </p>
     *
     * @param client - which contains the details that has to be
     *                        validated.
     *
     * @return status which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     */
    public String validateDetails(Client client) {     
        if (!Validation.validateName(client.getName())) {
            return Constants.NAME;
        }
        if (!Validation.validateEmail(client.getEmailId())) {
            return Constants.EMAIL;
        }
        return Constants.STATUS_OK;
    }

    /**
     * <p>
     * validating an specified attribute by the given data.
     * </p>
     *
     * @param attribute - which specifies the type of data.
     *
     * @param data - which has to be validated based on attribute type.
     * 
     * @return true if data is in specified format else return false.
     */
    public boolean validateAttribute(String attribute, String data) {      
        if (attribute.equals(Constants.NAME)) {
            return Validation.validateName(data);
        }
        if (attribute.equals(Constants.EMAIL)) {
            return Validation.validateEmail(data);
        } 
        return false;
    }
 }



