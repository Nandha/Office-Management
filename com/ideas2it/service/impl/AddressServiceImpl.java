package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.AddressDAO;
import com.ideas2it.dao.impl.AddressDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on Addresss details. The buisness 
 * logics such as adding an address to addresss, deleting an address from 
 * addresss and validating the information of an address(such as name & email)
 * are implemented.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 25/07/2017.
 */
public class AddressServiceImpl implements AddressService {
    private AddressDAO addressDao = new AddressDAOImpl();

    /**
     * @see com.ideas2it.service.AddressService#addAddress(Address).
     */
    public String addAddress(Address address) throws ApplicationException,
        UserInputException {
        if(null == address || addressDao.isHolderIdExists(address.getHolderId())){
            return " ";
        } else {
            if (validateDetailsOfAddress(address)) {
                addressDao.insertAddress(address);
            } else {
                throw new UserInputException(Constants.ERRCD_INVALID, null,
                                                address);
            }
        }
        return " ";
    }

    /**
     * @see com.ideas2it.service.AddressService#getAddresssByProjectId(String)
     */
    public Address getAddressesByHolderId(String holderId)
        throws ApplicationException {
        return addressDao.readAddressById(holderId);
    }

    /**
     * @see com.ideas2it.service.AddressService#updateAddress(String, String,
     *      String).
     */
    public String updateAddress(String holderId, String attribute,
                                     String data) throws ApplicationException {
        if (validateAttribute(attribute, data)) {
            if (addressDao.readAddressById(holderId).getHolderId() != null) {
                addressDao.updateAddress(holderId, getColumnName(attribute),
                                              data);
                return Constants.STATUS_OK;
            }
        } else {
            return attribute;
        }
        return " ";
    }


    /**
     * @see com.ideas2it.service.AddressService#deleteAddressById(String).
     */
    public boolean deleteAddressByHolderId(String id) throws ApplicationException {
        if (addressDao.readAddressById(id).getHolderId() != null) {
            return (addressDao.deleteAddressById(id) > 0);
        } else {
            return false;
        }
    }

    public List<String> getAllEmployees() throws ApplicationException {
        return (new EmployeeServiceImpl().getAllEmployees());
    }

    public List<Client> getAllClients() throws ApplicationException {
        return (new ClientServiceImpl().getAllClients());
    }

    /**
     * <p>
     * Getting attribute name of table address for the given attribute type.
     * </p>
     *  
     * @param attribute - which defines the attribute type.
     *
     * @return name of the address table attribute. 
     */
    private String getColumnName(String attribute) {
        if (attribute.equals(Constants.HOLDER_NAME)) {
            return Constants.ATTRIB_ADDR_NAME;
        } 
        if (attribute.equals(Constants.ADDRESS_ONE)) {
            return Constants.ATTRIB_ADDR_ONE;
        }
        if (attribute.equals(Constants.ADDRESS_TWO)) {
            return Constants.ATTRIB_ADDR_TWO;
        }
        if (attribute.equals(Constants.CITY)) {
            return Constants.ATTRIB_ADDR_CITY;
        }
        if (attribute.equals(Constants.ZIP)) {
            return Constants.ATTRIB_ADDR_ZIP;
        }
        if (attribute.equals(Constants.STATE)) {
            return Constants.ATTRIB_ADDR_STATE;
        }
        if (attribute.equals(Constants.COUNTRY)) {
            return Constants.ATTRIB_ADDR_COUNTRY;
        }
        if (attribute.equals(Constants.LANDMARK)) {
            return Constants.ATTRIB_ADDR_LMARK;
        }
        return " ";
    }

    /**
     * <p>
     * validating address's details such as name, email id, salary and DOB.
     * </p>
     *
     * @param address - which contains the details are to be validated.
     *
     * @return string which states whether validation is ok or which attribute 
     *                is not satisfying the requiered format.
     */
    private boolean validateDetailsOfAddress(Address address) {
        boolean valid = true;
        if (!Validation.validateName(address.getName())) {
            address.setName(Constants.INVALID);
            valid = false;
        }
        if (!Validation.isValidAddress(address.getAddressOne())) {
            address.setAddressOne(Constants.INVALID);
            valid = false;
        }
        if (!Validation.isValidAddress(address.getAddressTwo())) {
            address.setAddressTwo(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateName(address.getCity())) {
            address.setCity(Constants.INVALID);
            valid = false;
        }
        if (!Validation.isNumber(address.getZipcode())) {
            address.setZipcode(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateName(address.getState())) {
            address.setState(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateName(address.getCountry())) {
            address.setCountry(Constants.INVALID);
            valid = false;
        }
        return valid;
    }

    /**
     * <p>
     * validating an specified attribute of address.
     * </p>
     *
     * @param attribute - which specifies the type of data.
     * @param data - which has to be validated based on attribute type.
     * 
     * @return true if data is in specified format else return false.
     */
    private boolean validateAttribute(String attribute, String data) {
        if (attribute.equals(Constants.HOLDER_NAME) ||
               attribute.equals(Constants.CITY) ||
               attribute.equals(Constants.STATE) ||
               attribute.equals(Constants.COUNTRY)) {
            return Validation.validateName(data);
        } else if (attribute.equals(Constants.ADDRESS_ONE) ||
                      attribute.equals(Constants.ADDRESS_TWO) ||
                      attribute.equals(Constants.LANDMARK)) {
            return Validation.isValidAddress(data);
        } else if (attribute.equals(Constants.ZIP)) {
            return Validation.isNumber(data);
        }
        return false;
    }
 }



