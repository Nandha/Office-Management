package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.ProjectDAO;
import com.ideas2it.dao.impl.ProjectDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.ClientService;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on project details. The buisness 
 * logics such as adding a project to projects, deleting a project from 
 * projects and validating the information of an project(such as name) are 
 * implemented.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public class ProjectServiceImpl implements ProjectService {
    private ProjectDAO projectDao = new ProjectDAOImpl();
    private ClientService clientService = new ClientServiceImpl();

    /**
     * @see com.ideas2it.service.ProjectService#addProject(Project).
     */
    public String addProject(Project project) throws ApplicationException {
        if(null == project){
            return " ";
        } else {
            String status = validateDetails(project);
            if (status.equals(Constants.STATUS_OK)) {
                        projectDao.insertProject(project);
                    return Constants.STATUS_OK;
            } else {
                return status;
            }
        }
    }

    /**
     * @see com.ideas2it.service.ProjectService#getProject(String).
     */
    public Project getProject(String id) throws ApplicationException {
		return projectDao.readProjectById(id);
    }

    /**
     * @see com.ideas2it.service.ProjectService#getAllProjects().
     */
    public List<Project> getAllProjects() throws ApplicationException {
       return projectDao.readAllProject();
    }

    /**
     * @see com.ideas2it.service.ProjectService#getAllEmployees().
     */
    public List<String> getAllEmployees() throws ApplicationException {
        EmployeeService employeeService = new EmployeeServiceImpl();
        return employeeService.getAllEmployees();
    }

    /**
     * @see com.ideas2it.service.ProjectService#updateProject(String, String,
     *          String).
     */
    public String updateProject(String projectId, String attribute, String data
                                   ) throws ApplicationException {
        if (validateAttribute(attribute, data)) {
            attribute = getColumnName(attribute);
            if (projectDao.updateProject(projectId, attribute, data) > 0) {
                return Constants.STATUS_OK;
            }
        } else {
            return attribute;
        }
        return null;
    }

    /**
     * @see com.ideas2it.service.ProjectService#deleteProjectById(String).
     */
    public boolean deleteProjectById(String id) throws ApplicationException {
        return (projectDao.deleteProjectById(id) > 0);
    }

    /**
     * @see com.ideas2it.service.ProjectService#getIdByProjectName(String).
     */
    public String getIdByProjectName(String name) throws ApplicationException {
        return projectDao.readProjectIdByName(name);
    }

    public String getColumnName(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            return Constants.ATTRIB_PROJ_NAME;
        } else {
            return " ";
        }
    }

    /**
     * @return status which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     */
    public void assignEmployeesToProject(String employeeId, String projectId)
            throws ApplicationException, UserInputException {
        EmployeeService employeeService = new EmployeeServiceImpl();
        employeeService.updateEmployee(employeeId, Constants.PROJECT_ID,
                                          projectId);
    }

    /**
     * <p>
     * validating project's details such as name.
     * </p>
     *
     * @param project - which contains the details that has to be
     *                        validated.
     *
     * @return status which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     */
    public String validateDetails(Project project) {    
        if (!Validation.validateName(project.getName())) {
            return Constants.NAME;
        }
        if (clientService.getClient(project.getClient().getId()) == null) {
            return Constants.CLIENT;
        }
        return Constants.STATUS_OK;
    }

    /**
     * <p>
     * validating an specified attribute by the given data.
     * </p>
     *
     * @param attribute - which specifies the type of data.
     *
     * @param data - which has to be validated based on attribute type.
     * 
     * @return true if data is in specified format else return false.
     */
    public boolean validateAttribute(String attribute, String data) {
        
        if (attribute.equals(Constants.NAME)) {
            return Validation.validateName(data);
        } 
        return false;
    }
 }



