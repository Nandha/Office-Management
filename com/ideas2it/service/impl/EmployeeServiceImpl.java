package com.ideas2it.service.impl;

import java.util.List;
import java.util.ArrayList;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.EmployeeDAO;
import com.ideas2it.dao.impl.EmployeeDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.util.Calculation;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on employee details. The buisness 
 * logics such as adding an employee to employees, deleting an employee from 
 * employees and validating the information of an employee(such as name & email)
 * are implemented.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 25/07/2017.
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDAO employeeDao = new EmployeeDAOImpl();
    private ProjectService projectService = new ProjectServiceImpl();
    /**
     * @see com.ideas2it.service.EmployeeService#addEmployee(Employee).
     */
    public String addEmployee(Employee employee) throws ApplicationException,
        UserInputException {
        if(null == employee){
            return " ";
        } else {
            if (validateDetailsOfEmployee(employee)) {
                employee.setAge(Calculation.calculateAge(employee.
                                                            getDateOfBirth()));
                employeeDao.insertEmployee(employee);
            } else {
                throw new UserInputException(Constants.ERRCD_INVALID, null,
                                                employee);
            }
        }
        return " ";
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeeById(String).
     */
    public Employee getEmployeeById(String id) throws ApplicationException {
		return employeeDao.readEmployeeById(id);
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeeById(String).
     */
    public List<String> getProjectNames() throws ApplicationException {
        List<Project> allProjects = projectService.getAllProjects();
        List<String> projectNames = new ArrayList<String>(allProjects.size());
		for(Project project : allProjects)
            projectNames.add(project.getId() + " - " + project.getName());
        return projectNames;
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeesByProjectId(String)
     */
    public List<Employee> getEmployeesByProjectId(String projectId)
        throws ApplicationException {
        return employeeDao.readEmployeesByProjectId(projectId);
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeesByProjectId(String)
     */
    public List<String> getAllEmployees() throws ApplicationException {
        List<Employee> allEmployees = employeeDao.readAllEmployees();
        List<String> employeeNames = new ArrayList<String>(allEmployees.size());
		for(Employee employee : allEmployees)
            employeeNames.add(employee.getId() + " - " + employee.getName());
        return employeeNames;
    }

    /**
     * @see com.ideas2it.service.EmployeeService#updateEmployee(String, String,
     *      String).
     */
    public String updateEmployee(String employeeId, String attribute,
                                     String data) throws ApplicationException,
                                     UserInputException {
        if (validateAttribute(attribute, data)) {
            if(employeeDao.readEmployeeById(employeeId).getId() != null) {
                employeeDao.updateEmployee(employeeId, getColumnName(attribute),
                                              data);
                return Constants.STATUS_OK;
            } else {
                throw new UserInputException(Constants.EMPLOYEE_ID, null, null);
            } 
        }
         else {
            throw new UserInputException(Constants.PROJECT_ID, null, null); 
        }
    }

    public String getProjectIdByName(String name) throws ApplicationException {
        return projectService.getIdByProjectName(name);
    }

    /**
     * @see com.ideas2it.service.EmployeeService#deleteEmployeeById(String).
     */
    public boolean deleteEmployeeById(String id) throws ApplicationException {
        if (employeeDao.readEmployeeById(id).getId() != null) {
            return (employeeDao.deleteEmployeeById(id) > 0);
        } else {
            return false;
        }
    }

    /**
     * <p>
     * Getting attribute name of table employee for the given attribute type.
     * </p>
     *  
     * @param attribute - which defines the attribute type.
     *
     * @return name of the employee table attribute. 
     */
    private String getColumnName(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            return Constants.ATTRIB_EMP_NAME;
        } 
        if (attribute.equals(Constants.EMAIL)) {
            return Constants.ATTRIB_EMP_EMAIL;
        }
        if (attribute.equals(Constants.DOB)) {
            return Constants.ATTRIB_EMP_DOB;
        }
        if (attribute.equals(Constants.AGE)) {
            return Constants.ATTRIB_EMP_AGE;
        }
        if (attribute.equals(Constants.SALARY)) {
            return Constants.ATTRIB_EMP_SALARY;
        }
        if (attribute.equals(Constants.PROJECT_ID)) {
            return Constants.ATTRIB_PROJ_ID;
        }
        return " ";
    }

    /**
     * <p>
     * validating employee's details such as name, email id, salary and DOB.
     * </p>
     *
     * @param employee - which contains the details are to be validated.
     *
     * @return string which states whether validation is ok or which attribute 
     *                is not satisfying the requiered format.
     */
    private boolean validateDetailsOfEmployee(Employee employee) {
        boolean valid = true;
        if (!Validation.validateName(employee.getName())) {
            employee.setName(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateEmail(employee.getEmailId())) {
            employee.setEmailId(Constants.INVALID);
            valid = false;
        }
        if (!Validation.checkDateFormat(employee.getDateOfBirth())) {
            employee.setDateOfBirth(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateSalary(employee.getSalary())) {
            employee.setSalary(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateSalary(employee.getSalary())) {
            employee.setSalary(Constants.INVALID);
            valid = false;
        }
        if (projectService.getProject((employee.getProject().getId()))== null) {
            employee.setProject(null);
            valid = false;
        }
        return valid;
    }

    /**
     * <p>
     * validating an specified attribute of employee.
     * </p>
     *
     * @param attribute - which specifies the type of data.
     * @param data - which has to be validated based on attribute type.
     * 
     * @return true if data is in specified format else return false.
     */
    private boolean validateAttribute(String attribute, String data) {
        if (attribute.equals(Constants.NAME)) {
            return Validation.validateName(data);
        } else if (attribute.equals(Constants.EMAIL)) {
            return Validation.validateEmail(data);
        } else if (attribute.equals(Constants.DOB)) {
            return Validation.checkDateFormat(data);
        } else if (attribute.equals(Constants.SALARY)) {
            return Validation.validateSalary(data);
        } else if (attribute.equals(Constants.PROJECT_ID)) {
            return (projectService.getProject(data) != null);
        }
        return false;
    }
 }



