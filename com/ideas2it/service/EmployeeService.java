package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Employee;

/**
 * <p>
 * Implementing a Service for CRUD operation on employee details. The buisness 
 * logics such as adding an employee to employees, deleting an employee from 
 * employees and validating the information of an employee(such as name & email)
 * are implemented.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 25/07/2017.
 */
public interface EmployeeService {

    /** 
     * <p>
     * Adding an employee to employees.
     * </p>
     * 
     * @param employee - which is to be added in Employees.
     *
     * @return status which indicates the contoller whether employee is added or
     *                the details are not in the specified format.
     */
    public String addEmployee(Employee employee) throws UserInputException,
        ApplicationException;

    /** 
     * <p>
     * Reading an employee from employee's if the given id exists.
     * </p>
     * 
     * @param id - which is to be searched in Employees.
     * 
     * @return employee which is of given id.
     */
    public Employee getEmployeeById(String id) throws ApplicationException;

    /** 
     * <p>
     * Reading employees from employees, in a project specified by project Id.
     * </p>
     * 
     * @param projectId - which is to be matched with Employee's project id.
     * 
     * @return employees for which the project Id is matching with the given
     *                   project id.
     */
    public List<Employee> getEmployeesByProjectId(String projectId)
        throws ApplicationException;

    public String getProjectIdByName(String name) throws ApplicationException;

    /** 
     * <p>
     * Reading all the employees.
     * </p>
     * 
     * @return all the employees.
     *
     */
    public List<String> getAllEmployees() throws ApplicationException;

    /** 
     * <p>
     * Reading all project names.
     * </p>
     * 
     * @return all project names along with the project id.
     *                   
     */
    public List<String> getProjectNames() throws ApplicationException;

    /**
     * <p>
     * Updating the attribute of the given employee by the given data.
     * </p>
     *  
     * @param employeeId - indicates the employee for which updation is done.
     * @param attribute - is to be updated in an employee specified by the id.
     * @param data - which is to be replaced in the specified attribute of an 
     *               employee(specified by id).
     * 
     * @return status whether attribute of the given employee is updated by
     *             the given data else return the attribute as it is not 
     *             satisfying the format.
     */
    public String updateEmployee(String employeeId, String attribute,
                                     String data) throws ApplicationException,
                                     UserInputException;

    /**
     * <p>
     * Deleting an employee from employees by the given id.
     * </p>
     * 
     * @param id - which specifies the employee to be deleted from employees.
     * 
     * @return true if employee deleted from employees else return false.
     */
    public boolean deleteEmployeeById(String id) throws ApplicationException;

 }



