package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a Service for CRUD operation on Address details. The buisness 
 * logics such as adding an address, deleting an address and validating the 
 * information of an address(such as zipcode & holder name) are implemented.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 25/07/2017.
 */
public interface AddressService {

    /** 
     * <p>
     * Adding an address.
     * </p>
     * 
     * @param address - which is to be added in addresses.
     *
     * @return status which indicates the contoller whether address is added or
     *                the details are not in the specified format.
     */
    public String addAddress(Address address) throws UserInputException,
        ApplicationException;

    public List<String> getAllEmployees() throws ApplicationException;

    public List<Client> getAllClients() throws ApplicationException;

    /** 
     * <p>
     * Reading address by the given holder Id.
     * </p>
     * 
     * @param holderId - which is to be matched with address's holder id.
     * 
     * @return addresses for which the project Id is matching with the given
     *                   project id.
     */
    public Address getAddressesByHolderId(String holderId)
        throws ApplicationException;

    /**
     * <p>
     * Updating the attribute of the given address by the given data.
     * </p>
     *  
     * @param holderId - indicates the address for which updation is done.
     * @param attribute - is to be updated in an address specified by the id.
     * @param data - which is to be replaced in the specified attribute of an 
     *               address(specified by id).
     * 
     * @return status whether attribute of the given address is updated by
     *             the given data else return the attribute as it is not 
     *             satisfying the format.
     */
    public String updateAddress(String holderId, String attribute,
                                     String data) throws ApplicationException;

    /**
     * <p>
     * Deleting an address from addresses by the given holder id.
     * </p>
     * 
     * @param holderId - which specifies the address to be deleted.
     * 
     * @return true if address is deleted else return false.
     */
    public boolean deleteAddressByHolderId(String holderId) throws
                                                     ApplicationException;

 }



