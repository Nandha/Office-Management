package com.ideas2it.view;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
public class ClientView {

    /**
     * Showing user a set of options to choose.
     */
    public void showClientCRUDMenu() {
        UI.display(Constants.CLIENT_CRUD_MENU);
    }

    /**
     * Showing user a set of options to choose.
     */
    public void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }

    /**
     * Getting client details from user.
     *
     * @return client which has client details entered by user.
     */
    public Client getClientDetailsFromUser() {
        return (new Client(UI.getUserInput(Constants.CLIENT_ID),
                   UI.getUserInput(Constants.CLIENT_NAME),
                   UI.getUserInput(Constants.EMAIL)));
    }

    /**
     * Displaying details of the given client to the user.
     * 
     * @param client - which is to be displayed to the user.
     */
    public void displayClient(List<Client> clients) {
        for (Client client : clients)
            UI.display(String.format(Constants.DISPLAY_CLIENT,
                                        client.getId(), client.getName(),
                                        client.getEmailId()));
    }

    /**
     * Displaying details of the given client to the user.
     * 
     * @param client - which is to be displayed to the user.
     */
    public void displayClient(Client client) {
        if ( null == client) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants
                                        .CLIENT));
        } else {
            UI.display(String.format(Constants.DISPLAY_CLIENT,
                                        client.getId(), client.getName(),
                                        client.getEmailId()));
        }
    }

    /**
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     */
    public void showCorrectFormat(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            UI.display(Constants.MSG_NAME_FORMAT);
        } else if (attribute.equals(Constants.EMAIL)) {
            UI.display(Constants.MSG_EMAIL_FORMAT);
        } else if (attribute.equals(Constants.DOB)) {
            UI.display(Constants.MSG_DATE_FORMAT);
        } else if (attribute.equals(Constants.SALARY)) {
            UI.display(Constants.MSG_SALARY_FORMAT);
        }
    }


    /**
     * Showing user a set of options to choose for updation.
     */
    public void showClientUpdateMenu() {
        UI.display(Constants.CLIENT_UPD_MENU);
    }

    /**
     * Showing user by the given message.
     */
    public void showMessage(String message) {
        UI.display(message);
    }
}
