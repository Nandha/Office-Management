package com.ideas2it.view;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
public class EmployeeView {

    /**
     * Showing user a set of options to choose.
     */
    public void showEmployeeCRUDMenu() {
        UI.display(Constants.EMP_CRUD_MENU);
    }

    /**
     * Showing user a set of options to choose.
     */
    public static void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }


    /**
     * Getting employee details from user.
     *
     * @return employee which has employee details entered by user.
     */
    public Employee getEmployeeDetailsFromUser() {
        return (new Employee(UI.getUserInput(Constants.ID),
                    UI.getUserInput(Constants.NAME),
                    UI.getUserInput(Constants.EMAIL),
                    UI.getUserInput(Constants.DOB), 0,
                    UI.getUserInput(Constants.SALARY),
                    new Project(UI.getUserInput(Constants.PROJECT_ID), null,
                                   null)));
    }

   /**
     * Getting valid employee details from user.
     *
     * @return employee which has valid employee details entered by user.
     */
    public Employee getValidEmployeeDetailsFromUser(Employee employee) {
        if (employee.getId().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.ID);
            employee.setId(UI.getUserInput(Constants.ID));
        }
        if (employee.getName().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            employee.setName(UI.getUserInput(Constants.NAME));
        }
        if (employee.getEmailId().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.EMAIL);
            employee.setEmailId(UI.getUserInput(Constants.EMAIL));
        }
        if (employee.getDateOfBirth().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.DOB);
            employee.setDateOfBirth(UI.getUserInput(Constants.DOB));
        }
        if (employee.getSalary().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.SALARY);
            employee.setSalary(UI.getUserInput(Constants.SALARY));
        }
        if (employee.getProject() == null) {
            employee.setProject(new Project(UI.getUserInput
                                     (Constants.PROJECT_ID), null, null));
        }
        return employee;
    }

    /**
     * Displaying details of the given employee to the user.
     * 
     * @param employee - which has details to be displayed to the user.
     */
    public void displayEmployee(Employee employee) {
        if ( null == employee.getId()) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants.
                          EMPLOYEE));
        } else {
            UI.display(String.format(Constants.DISPLAY_EMPLOYEE, employee.
                           getId(), employee.getName(), employee.getEmailId(),
                           employee.getDateOfBirth(), employee.getSalary(), 
                           employee.getAge(), employee.getProject().getId()));
        }
    }

    /**
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     */
    public void showCorrectFormat(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            UI.display(Constants.MSG_NAME_FORMAT);
        } else if (attribute.equals(Constants.EMAIL)) {
            UI.display(Constants.MSG_EMAIL_FORMAT);
        } else if (attribute.equals(Constants.DOB)) {
            UI.display(Constants.MSG_DATE_FORMAT);
        } else if (attribute.equals(Constants.SALARY)) {
            UI.display(Constants.MSG_SALARY_FORMAT);
        }
    }

    /**
     * Showing user a set of options to choose for updation.
     */
    public void showEmployeeUpdateMenu() {
        UI.display(Constants.EMP_UPD_MENU);
    }

    /**
     * Showing user by the given message.
     */
    public void showMessage(String message) {
        UI.display(message);
    }

    /**
     * Showing user by the given message.
     */
    public void showMessage(List<String> messages) {
        int count = 1;
        for(String message: messages)
        UI.display(count++ + ". "+message);
    }
}
