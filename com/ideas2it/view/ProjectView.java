package com.ideas2it.view;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.model.Client;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
public class ProjectView {

    /**
     * Showing user a set of options to choose.
     */
    public void showProjectCRUDMenu() {
        UI.display(Constants.PROJECT_CRUD_MENU);
    }

    /**
     * Showing user a set of options to choose.
     */
    public void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }

    /**
     * Getting project details from user.
     *
     * @return project which has project details entered by user.
     */
    public Project getProjectDetailsFromUser() {
        return (new Project(UI.getUserInput(Constants.PROJECT_ID),
                   UI.getUserInput(Constants.PROJECT_NAME), new Client(UI.
                   getUserInput(Constants.CLIENT_ID), null, null)));
    }

    /**
     * Displaying details of the given project to the user.
     * 
     * @param project - which is to be displayed to the user.
     */
    public void displayProject(Project project) {
        if ( null == project) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants
                              .PROJECT));
        } else {
            UI.display(String.format(Constants.DISPLAY_PROJECT,
                                        project.getId(), project.getName(),
                                        project.getClient().getId()));
        }
    }

    /**
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     */
    public void showCorrectFormat(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            UI.display(Constants.MSG_NAME_FORMAT);
        } else if (attribute.equals(Constants.EMAIL)) {
            UI.display(Constants.MSG_EMAIL_FORMAT);
        } else if (attribute.equals(Constants.DOB)) {
            UI.display(Constants.MSG_DATE_FORMAT);
        } else if (attribute.equals(Constants.SALARY)) {
            UI.display(Constants.MSG_SALARY_FORMAT);
        }
    }


    /**
     * Showing user a set of options to choose for updation.
     */
    public void showProjectUpdateMenu() {
        UI.display(Constants.PROJECT_UPD_MENU);
    }

    /**
     * Showing user by the given message.
     */
    public void showMessage(String message) {
        UI.display(message);
    }

    /**
     * Showing user by the given message.
     */
    public void showMessage(List<String> messages) {
        int count = 1;
        for(String message: messages)
        UI.display(count++ + ". "+message);
    }
}
