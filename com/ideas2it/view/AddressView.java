package com.ideas2it.view;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.model.Address;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 18/07/2017.
 */
public class AddressView {

    /**
     * Showing user a set of options to choose.
     */
    public void showAddressCRUDMenu() {
        UI.display(Constants.ADD_CRUD_MENU);
    }

    /**
     * Showing user a set of options to choose.
     */
    public static void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }


    /**
     * Getting address details from user.
     *
     * @return address which has address details entered by user.
     */
    public Address getAddressDetailsFromUser() {
        return (new Address(null, UI.getUserInput(Constants.HOLDER_NAME),
                    UI.getUserInput(Constants.ADDRESS_ONE),
                    UI.getUserInput(Constants.ADDRESS_TWO),
                    UI.getUserInput(Constants.CITY),
                    UI.getUserInput(Constants.ZIP),
                    UI.getUserInput(Constants.STATE),
                    UI.getUserInput(Constants.COUNTRY),
                    UI.getUserInput(Constants.LANDMARK)));
    }

   /**
     * Getting valid address details from user.
     *
     * @return address which has valid address details entered by user.
     */
    public Address getValidAddressDetailsFromUser(Address address) {
        if (address.getHolderId().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.ID);
            address.setHolderId(UI.getUserInput(Constants.HOLDER_ID));
        }
        if (address.getName().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            address.setName(UI.getUserInput(Constants.HOLDER_NAME));
        }
        if (address.getCity().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.EMAIL);
            address.setCity(UI.getUserInput(Constants.CITY));
        }
        if (address.getAddressTwo().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.DOB);
            address.setAddressTwo(UI.getUserInput(Constants.ADDRESS_TWO));
        }
        if (address.getZipcode().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.SALARY);
            address.setZipcode(UI.getUserInput(Constants.ZIP));
        }
        return address;
    }

    /**
     * Displaying details of the given address to the user.
     * 
     * @param address - which has details to be displayed to the user.
     */
    public void displayAddress(Address address) {
        if ( null == address.getHolderId()) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants.
                          HOLDER_ID));
        } else {
            UI.display(String.format(Constants.DISPLAY_ADDRESS, address.
                           getHolderId(), address.getName(),
                           address.getAddressOne(), address.getAddressTwo(),
                           address.getCity(), address.getZipcode(),
                           address.getState(), address.getCountry(),
                           address.getLandMark()));
        }
    }

    /**
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     */
    public void showCorrectFormat(String attribute) {
        showMessage(Constants.MSG_ADDRESS_FORMAT);
    }

    /**
     * Showing user a set of options to choose for updation.
     */
    public void showAddressUpdateMenu() {
        UI.display(Constants.ADDR_UPD_MENU);
    }

    /**
     * Showing user by the given message.
     */
    public void showMessage(String message) {
        UI.display(message);
    }

    /**
     * Showing user by the given messages.
     */
    public void showMessage(List<String> messages) {
        int count = 1;
        for(String message: messages)
        UI.display(count++ + ". "+message);
    }
}
