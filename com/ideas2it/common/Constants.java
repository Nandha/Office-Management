package com.ideas2it.common;

/**
 * <p>
 * All the constants that are used in the view and controller classes have been 
 * given a variable name with constant.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 22/07/2017.
 */
public class Constants {
    /* For Validation purpose. */
    public static final String NAME_FORMAT = "[a-zA-Z ]+";
    public static final String EMAIL_FORMAT = "[a-zA-Z0-9@.]+";
    public static final String ADDR_FORMAT = "[a-zA-Z0-9/., ]+";
    public static final String NUM_FORMAT = "[0-9 ]+";
    public static final char AT = '@';
    public static final String DOMAIN_COM = ".com";
    public static final String DOMAIN_NET = ".net";
    public static final String REGEX_NUM = "[0-9]+";
    public static final int MIN_SALARY = 1000;
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final char YES_LOWER = 'y';
    public static final char YES = 'Y';

    /* For Database Connectivity purpose. */
    public static final String ATTRIB_EMP_ID = "emp_id";
    public static final String ATTRIB_EMP_NAME = "emp_name";
    public static final String ATTRIB_EMP_EMAIL = "email_id";
    public static final String ATTRIB_EMP_DOB = "dob";
    public static final String ATTRIB_EMP_AGE = "age";
    public static final String ATTRIB_EMP_SALARY = "salary";
    public static final String ATTRIB_PROJ_ID = "project_id";
    public static final String ATTRIB_PROJ_NAME = "project_name";
    public static final String ATTRIB_CLIENT_ID = "client_id";
    public static final String ATTRIB_CLIENT_NAME = "client_name";
    public static final String ATTRIB_CLIENT_EMAIL = "email_id";
    public static final String ATTRIB_ADDR_ID = "holder_id";
    public static final String ATTRIB_ADDR_NAME = "name";
    public static final String ATTRIB_ADDR_ONE = "add_line_one";
    public static final String ATTRIB_ADDR_TWO = "add_line_two";
    public static final String ATTRIB_ADDR_LMARK = "landmark";
    public static final String ATTRIB_ADDR_CITY = "city";
    public static final String ATTRIB_ADDR_ZIP = "zipcode";
    public static final String ATTRIB_ADDR_STATE = "state";
    public static final String ATTRIB_ADDR_COUNTRY = "country";
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    public static final String DB_MYSQL_URL = "jdbc:mysql://localhost/"
                                              + "employee_db?autoReconnect=true"
                                              + "&useSSL=false";
    public static final String MYSQL_USER = "root";
    public static final String MYSQL_PASS = "root";
    public static final String EMP_INS_QRY = "insert into employee "
                                             + "values('%s', '%s', "
                                             + "'%s', '%s', '%s', '%s', '%s')";

    public static final String EMP_DEL_QRY = "delete from employee where "
                                             + "emp_id ='%s';";

    public static final String EMP_UPD_QRY = "update employee set %s "
                                            + "='%s' where emp_id ='%s';";

    public static final String EMP_READ_QRY = "select * from employee where"
                                              + " emp_id ='%s';";

    public static final String EMP_READ_ALL_QRY = "select * from employee;";

    public static final String EMP_READ_BY_PROJ_QRY = "select * from employee "
                                                      + "where project_id='%s'";

    public static final String PROJ_INS_QRY = "insert into project values"
                                              + "('%s', '%s', '%s');";

    public static final String PROJ_READ_QRY = "select * from project where "
                                               + "project_id ='%s';";

    public static final String PROJ_DEL_QRY = "delete from project where "
                                              + "project_id ='%s';";

    public static final String PROJ_UPD_QRY = "update project set %s ='%s' "
                                              + "where project_id = '%s';";

    public static final String PROJ_ID_QRY = "select project_id from project "
                                             + "where project_name ='%s';";

    public static final String PROJ_READ_ALL_QRY = "select * from project;";


    public static final String CLIENT_INS_QRY = "insert into client values"
                                              + "('%s', '%s', '%s');";

    public static final String CLIENT_READ_QRY = "select * from client where "
                                               + "client_id ='%s';";

    public static final String CLIENT_DEL_QRY = "delete from client where "
                                              + "client_id ='%s';";

    public static final String CLIENT_UPD_QRY = "update client set %s ='%s' "
                                              + "where client_id = '%s';";

    public static final String CLIENT_ID_QRY = "select client_id from client "
                                             + "where client_name ='%s';";

    public static final String CLIENT_READ_ALL_QRY = "select * from client;";

    public static final String ADDR_INS_QRY = "insert into address "
                                             + "values('%s', '%s', '%s', '%s',"
                                             + "'%s', '%s', '%s', '%s', '%s')";

    public static final String ADDR_DEL_QRY = "delete from address where "
                                             + "holder_id ='%s';";

    public static final String ADDR_UPD_QRY = "update address set %s "
                                            + "='%s' where holder_id ='%s';";

    public static final String ADDR_READ_QRY = "select * from address where"
                                              + " holder_id ='%s';";

    /* For User Interface purpose. */
    public static final String STATUS_OK = "ok";
    public static final String STATUS_IDEXIST = "ID Exist";
    public static final String NAME = "Name";
    public static final String ID = "Id";
    public static final String EMAIL = "Email";
    public static final String DOB = "Date of Birth";
    public static final String AGE = "Age";
    public static final String SALARY = "Salary";
	public static final String PROJECT = "Project";
	public static final String EMPLOYEE = "Employee";
	public static final String CLIENT = "Client";
	public static final String EMPLOYEE_ID = "Employee ID";
	public static final String HOLDER_ID = "Address Holder Id";
	public static final String HOLDER_NAME = "Addess Holder Name";
    public static final String ADDRESS_ONE = "Address (lane, Street, Area) :";
    public static final String ADDRESS_TWO = "Address (Apartments , Flat "
                                             + ", Floor)";
    public static final String LANDMARK = "Landmark :";
    public static final String CITY = "City";
    public static final String ZIP = "Zip Code";
    public static final String STATE = "State";
    public static final String COUNTRY = "Country";
    public static final String PROJECT_ID = "Project ID";
    public static final String PROJECT_NAME = "Project Name";
    public static final String CLIENT_ID = "Client ID";
    public static final String CLIENT_NAME = "Client Name";
    public static final String ON_BENCH = "00000";

    public static final String EMP_ID = "Employee Id";
    public static final int MINIMUM_EMPLOYEE_ATTRIBUTE = 6;
    public static final int MINIMUM_PROJET_ATTRIBUTE = 2;

    public static final String OPTION_INVALID_MSG = "\n*** Invalid Option"
                                                    + " Entered *** \n Please "
                                                    + "choose Correct option.";

	public static final String MAIN_MENU = "\nEnter Your choice:"
	                                       + "\n1. Employee management"
                                           + "\n2. Project management"
                                           + "\n3. Client management"
                                           + "\n3. Address management"
										   + "\n0. Exit\n";

    public static final String EMP_CRUD_MENU = "\nEnter Your choice:"
	                                           + "\n1. Add an Employee"
                                               + "\n2. Display Employee"
                                               + "\n3. Update an Employee" 
                                               + "\n4. Delete an Employee"
											   + "\n5. Assign project to an"
                                               + " Employee"
											   + "\n6. Unassign project from a"
                                               + "Employee"
											   + "\n7. List Employees in a"
                                               + " Project"
                                               + "\n0. Exit\n";
											   
    public static final String PROJECT_CRUD_MENU = "\nEnter Your choice:"
	                                           + "\n1. Add a Project"
                                               + "\n2. Display Project"
                                               + "\n3. Update a Project" 
                                               + "\n4. Delete a Project"
											   + "\n5. Assign Employees to a"
                                               + " project"
                                               + "\n0. Exit\n";
	
    public static final String ADD_CRUD_MENU = "\nEnter Your choice:"
	                                           + "\n1. Add an Address"
                                               + "\n2. Display an Address"
                                               + "\n3. Update an Address" 
                                               + "\n4. Delete an Address"
                                               + "\n0. Exit\n";

    public static final String DISPLAY_EMPLOYEE = "\nEmployee Details:::"
                                                   + "\nID                : %s"
                                                   + "\nName              : %s" 
                                                   + "\nEmail ID          : %s" 
                                                   + "\nDate of Birth     : %s" 
                                                   + "\nSalary            : %s"
                                                   + "\nAge               : %s"
                                                   + "\nProject ID        : %s";

    public static final String DISPLAY_ADDRESS = "\nADDRESS Details:::"
                                                   + "\nHOLDER ID         : %s"
                                                   + "\nName              : %s" 
                                                   + "\nADDRESS LINE ONE  : %s" 
                                                   + "\nADDRESS LINE TWO  : %s" 
                                                   + "\nCITY              : %s"
                                                   + "\nZIPCODE           : %s"
                                                   + "\nSTATE             : %s"
                                                   + "\nCOUNTRY           : %s"
                                                   + "\nLANDMARK          : %s";

    public static final String CLIENT_CRUD_MENU = "\nEnter Your choice:\n"
                                               + "1. Add a client\n2. Display"
                                               + " client\n3. Update a client"
                                               + "\n4. Delete a client\n" 
                                               + "0. Exit\n";

    public static final String DISPLAY_CLIENT = "\n CLIENT Details:::"
                                                   + "\nID                : %s"
                                                   + "\nName              : %s" 
                                                   + "\nEmail ID          : %s"; 

    public static final String PROJECT_UPD_MENU = "\nUpdation Menu:\n1. Name"
                                                   +"\n0. Exit\n";

    public static final String ADDR_UPD_MENU = "\nUpdation Menu:\n1. Name"
                                               + "\n1. Address holder name"
                                               + "\n2. Address LINE ONE "
                                               + "\n3. Address LINE TWO "
                                               + "\n4. City "
                                               + "\n5. Zip code "
                                               + "\n6. State "
                                               + "\n7. Country "
                                               + "\n8. Land Mark "
                                               +"\n0. Exit\n";

    public static final String CLIENT_UPD_MENU = "\nUpdation Menu:\n1. Name\n2."
                                                 + " Email ID\n0. Exit\n";

    public static final String DISPLAY_PROJECT = "\nEmployee Details:::"
                                                 + "\nID                : %s"
                                                 + "\nName              : %s"
                                                 + "\nClient ID         : %s";

    public static final String EMP_UPD_MENU = "\nUpdation Menu:\n1. Name\n2. "
                                              + "Email ID\n3. Date of Birth\n"
                                              + "4. Salary\n0. Exit\n";
    public static final String MSG_ADDRESS_FORMAT = "Address fields can not "
                                                    + "have special cahracters";
    public static final String MSG_NAME_FORMAT = "Name should contain only"
                                                 +" alphabet and Space.";

    public static final String MSG_DATE_FORMAT = " Date should be given as "
                                                 + "YYYY-MM-DD.";

    public static final String MSG_EMAIL_FORMAT = "Mail Id should be as EX:- "
                                                  + "nandha94@gmail.com.";

    public static final String MSG_SALARY_FORMAT = "Salary should be greater"
                                                   + " than 1000.";
   public static final String MSG_NOT_EXIST = " is not existing in the"
                                               +" record";


    public static final String MSG_SUCCESS = "\n%s added succesfully";

    public static final String MSG_CHECK_FORMAT = "**** Check the format of %s";

    public static final String MSG_DELETION = "\n%s(%s) Deleted Successfully";

    public static final String MSG_PROCESS_DONE = "Process done succesfully";

    public static final String MSG_ENTER = "\nENTER %s :";

    public static final String MSG_CONFIRM = "\nDo you want to do the "
                                             +"Action????\n - Press y for yes"
                                             +" & any other key for No)";


    public static final String MSG_INS_SUCC = " - inserted succesfully.";
    public static final String MSG_INS_UNSUCC = " - was not inserted.";
    public static final String MSG_UPD_SUCC = " - updated succesfully.";
    public static final String MSG_UPD_UNSUCC = " - was not updated.";
    public static final String MSG_DEL_SUCC = " - updated succesfully.";

    public static final String INVALID = " INVALID ";
    public static final String ERRCD_INVALID = "ERR_INVALID ";
    public static final String ERRCD_ID_EXIST = "DUP_ID";
    public static final String ERRCD_ID_NOT_EXIST = "NOT_EXIST";
    public static final String ERRCD_UNABLE = "UNABLE";
    public static final String ERR_CLASS_MYSQL = "Error while loading Mysql"
                                                 + " Driver class";
    public static final String DEV_ERR_MYSQL_CHECK = "Check the DB name or "
                                                     + "credentials(Username or"
                                                     + " password) for MySQL.";
    public static final String DEV_ERR_MYSQL_UP = "The MySQL server is not "
                                                  + "Running up... Please start"
                                                  + " the server...";
    public static final String ERR_SINGLE_CONN = "Error while getting singleton"
                                                 + " connection ";
    public static final String DEV_ERR_CONN = "- Exception caused while creati"
                                              + " ng DB connection using URL - "
                                              + DB_MYSQL_URL + ", user - "
                                              + MYSQL_USER + " password - "
                                              + MYSQL_PASS;
    public static final String DEV_ERR_EXE_QUERY = "- Exception caused while "
                                                   + "executing the query : ";
    public static final String USR_ERR_INVALID_INPUT = "The following data's "
                                                       + "are not valid. please"
                                                       + "give them in the "
                                                       + "specified format.";
    public static final String USR_ERR_ID_EXIST = "The given %s  is alredy exis"
                                                  + "ting, please try another.";
    public static final String USR_ERR_ID_NOT_EXIST = "The given %s is not exis"
                                                      + "ting, please give exis"
                                                      + "ting one.";
    public static final String USR_ERR_UNABLE = "Unable to do the process due "
                                                + "to some problem.";
    public static final String PROJ_RD_ERR = "Error while reading project by "
                                             + "the given id.";
    public static final String PROJ_RD_ALL_ERR = "Error while reading all "
                                                 + "project's -";
    public static final String PROJ_RD_ID_ERR = "Error while reading project "
                                                + "id, by the project name -";
    public static final String PROJ_UPD_ERR = "Error while updating an project "
                                             + "by the id -%s in the attribute"
                                             + " -%s by the data -%s";
    public static final String PROJ_DEL_ERR = "Error when deleting a project,"
                                              + " by the id -";
    public static final String PROJ_INS_ERR = "Error while adding a project, "
                                              + "of details -";

    public static final String CLIENT_RD_ERR = "Error while reading project by "
                                               + "the given id.";
    public static final String CLIENT_RD_ALL_ERR = "Error while reading all "
                                                   + "project's -";
    public static final String CLIENT_RD_ID_ERR = "Error while reading project "
                                                + "id, by the project name -";
    public static final String CLIENT_UPD_ERR = "Error while updating project "
                                             + "by the id -%s in the attribute"
                                             + " -%s by the data -%s";
    public static final String CLIENT_DEL_ERR = "Error when deleting a project,"
                                              + " by the id -";
    public static final String CLIENT_INS_ERR = "Error while adding a project, "
                                              + "of details -";


    public static final String EMP_RD_ERR = "Error while reading employee by "
                                             + "the given id.";
    public static final String EMP_RD_ALL_ERR = "Error while reading all "
                                                 + "employee's -";
    public static final String EMP_RD_ALL_PROJ_ERR = "Error while reading all "
                                                     + "employee's by the "
                                                     + "project id -";
    public static final String EMP_UPD_ERR = "Error while updating an employee "
                                             + "by the id -%s in the attribute"
                                             + " -%s by the data -%s";
    public static final String EMP_DEL_ERR = "Error when deleting an employee,"
                                              + " by the id -";
    public static final String EMP_INS_ERR = "Error while adding an employee, "
                                              + "of details -";
    public static final String EMP_ASSIGN_PROJ_ERR = "Error while assigning a "
                                                     +"project to the employee";
    public static final String EMP_UNASSIGN_PROJ_ERR = "Error when removing a "
                                                     + "project from employee.";
    public static final String LOG4J_CONFIG_FILE = "log4jConfiguration.xml";
}
