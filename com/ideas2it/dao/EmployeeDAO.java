package com.ideas2it.dao;

import java.util.List;
import java.sql.ResultSet;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Employee;

/**
 * <p>
 * Data access object for employee entity.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 22/07/2017.
 */
public interface EmployeeDAO {

    /**
     * <p>
     * Inserting Employee details into the employee table.
     * </p>
     *
     * @param employee - which contains the details of an employee.
     *
     * @return the number of employees inserted.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            an employee detail.
     */
    public int insertEmployee(Employee employee) throws ApplicationException;

	/**
     * <p>
     * Reading employee details from the employee table by the given id.
     * </p>
     * 
     * @param employeeId - by which the details of an employee is read from the 
     *                     table.
     *
     * @return employee which is of the given emailId.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            an employee detail by the given employee id.
     */
	public Employee readEmployeeById(String employeeId) throws
        ApplicationException;

	/**
     * <p>
     * Reading employee's by the given project id.
     * </p>
     * 
     * @param projectId - by which the details of an employee is read.
     *
     * @return employees which has the given project id.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            all the employees in a project by the given project id.
     */
    public List<Employee> readEmployeesByProjectId(String projectId) throws
        ApplicationException;

	/**
     * <p>
     * Reading all the employee's.
     * </p>
     *
     * @return employees which has the given project id.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            all the employees in a project by the given project id.
     */
    public List<Employee> readAllEmployees() throws ApplicationException;

    /**
     * <p>
     * Updating employee details in the employee table by the given information.
     * </p>
     *
     * @param projectId - which specifies the employee to be updated.
     * @param attribute - which specifies the attribute of employee to be
     *                    updated.
     * @param data - which is updated in the specified attribute of the employee
     *
     * @return the number of rows updated in the employee table.
     *
     * @exception ApplicationException - if a SQL error occurs, when deleting
     *            a employee by the given employee id.
     */
    public int deleteEmployeeById(String employeeId) throws
        ApplicationException;

	/**
     * <p>
     * Deleting an employee from the employee table by the given id.
     * </p>
     * 
     * @param employeeId - by which an employee is deleted from the table.
     *
     * @return the number of rows deleted in the employee table.
     *
     * @exception ApplicationException - if a SQL error occurs, when updating
     *            an employee specified by the employee id by the given data.
     */
    public int updateEmployee(String employeeId, String attribute, String data)
        throws ApplicationException;
}
