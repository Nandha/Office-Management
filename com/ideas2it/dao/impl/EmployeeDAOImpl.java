package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.ConnectionFactory;
import com.ideas2it.dao.EmployeeDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a data access object for the entity employee to do CRUD 
 * operation in database. Such as inserting details of an employee into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 27/07/2017.
 */
public class EmployeeDAOImpl implements EmployeeDAO {

    private Connection connection = null;

    /**
     * @see com.ideas2it.dao.EmployeeDAO#insertEmployee(Employee).
     */
    @Override
    public int insertEmployee(Employee employee) throws
            ApplicationException {
        String query = String.format(Constants.EMP_INS_QRY, employee.getId(),
                          employee.getName(), employee.getEmailId(),
                          employee.getDateOfBirth(), employee.getAge(), Integer.
                          parseInt(employee.getSalary()),
                          employee.getProject().getId());
        return updateByQuery(query);
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#updateEmployee(String, String, String).
     */
    @Override
    public int updateEmployee(String employeeId, String attribute, String data)
                                            throws ApplicationException {
        String query = String.format(Constants.EMP_UPD_QRY, attribute, data,
                                        employeeId);
        return updateByQuery(query);
    }

	/**
     * @see com.ideas2it.dao.EmployeeDAO#deleteEmployeeById(String).
     */
    @Override
    public int deleteEmployeeById(String employeeId) throws 
                                                      ApplicationException {
        String query = String.format(Constants.EMP_DEL_QRY, employeeId);
        return updateByQuery(query);
    }

	/**
     * Executing the given query(which contains the given data).
     *
     * @param query - which is to be executed.
     *
     * @return the number of objects affected
     */
    private int updateByQuery(String query) {
        Statement statement = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY + query, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#readEmployeeById(String).
     */
    public Employee readEmployeeById(String employeeId) throws
                                                     ApplicationException {
        String query = String.format(Constants.EMP_READ_QRY, employeeId);
        ResultSet resultSet = readDataByQuery(query);
        try {
            if (resultSet.next()) {
                return createEmployee(resultSet);
            }
            return new Employee();
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR + employeeId, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#readEmployeeById(String).
     */
    public List<Employee> readAllEmployees() throws ApplicationException {
        ResultSet resultSet = readDataByQuery(Constants.EMP_READ_ALL_QRY);
        List<Employee> employeesInProject = null;
        try {
            employeesInProject = new ArrayList<Employee> (getRowsCount
                                                             (resultSet));
            while(resultSet.next()) {
                employeesInProject.add(createEmployee(resultSet));
            }
            return employeesInProject;
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#readEmployeesByProjectId(String).
     */
    @Override
    public List<Employee> readEmployeesByProjectId(String projectId) throws
                                                      ApplicationException {
        List<Employee> employeesInProject = null;
        String query = String.format(Constants.EMP_READ_BY_PROJ_QRY, projectId);
        try {
            ResultSet resultSet = readDataByQuery(query);
            employeesInProject = new ArrayList<Employee> (getRowsCount
                                                             (resultSet));
            while(resultSet.next()) {
                employeesInProject.add(createEmployee(resultSet));
            }
            return employeesInProject;
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.EMP_RD_ALL_ERR, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#createEmployee(ResultSet).
     */
    private Employee createEmployee(ResultSet resultSet) throws
                                                    SQLException {
        try {
            return (new Employee(resultSet.getString(Constants.ATTRIB_EMP_ID),
                         resultSet.getString(Constants.ATTRIB_EMP_NAME),
                         resultSet.getString(Constants.ATTRIB_EMP_EMAIL),
                         resultSet.getString(Constants.ATTRIB_EMP_DOB),
                         resultSet.getInt(Constants.ATTRIB_EMP_AGE),
                         resultSet.getString(Constants.ATTRIB_EMP_SALARY),
                         new Project(resultSet.getString(Constants.
                                        ATTRIB_PROJ_ID), null, null)));
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY, e);
            throw new ApplicationException(e);
        }
    }

	/**
     * Executing the given query(which contains the given data).
     *
     * @param query - which is to be executed.
     *
     * @return the read data object.
     */
    private ResultSet readDataByQuery(String query) {
        Statement statement = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY + query, e);
            throw new ApplicationException(e);
        }
    }

	/**
     * Counting the number of objects.
     *
     * @param resultSet - which has n of objects.
     *
     * @return the number of objects.
     */
    private int getRowsCount(ResultSet resultSet) {
        try {
            resultSet.last();
            int count = resultSet.getRow();
            resultSet.beforeFirst();
            return count;
        } catch(SQLException e){
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY, e);
            throw new ApplicationException(e);
        }
    }
}
