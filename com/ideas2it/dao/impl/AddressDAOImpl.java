package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.ConnectionFactory;
import com.ideas2it.dao.AddressDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Address;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a data access object for the entity address to do CRUD 
 * operation in database. Such as inserting details of an address into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 27/07/2017.
 */
public class AddressDAOImpl implements AddressDAO {

    private Connection connection = null;

    /**
     * @see com.ideas2it.dao.AddressDAO#insertAddress(Address).
     */
    @Override
    public int insertAddress(Address address) throws
            ApplicationException {
        String query = String.format(Constants.ADDR_INS_QRY, address.
                           getHolderId(), address.getName(),
                           address.getAddressOne(),address.getAddressTwo(),
                           address.getCity(), address.getZipcode(),
                           address.getState(), address.getCountry(),
                           address.getLandMark());
        return updateByQuery(query);
    }

    /**
     * @see com.ideas2it.dao.AddressDAO#updateAddress(String, String, String).
     */
    @Override
    public int updateAddress(String holderId, String attribute, String data)
                                            throws ApplicationException {
        String query = String.format(Constants.ADDR_UPD_QRY, attribute, data,
                                        holderId);
        return updateByQuery(query);
    }

	/**
     * @see com.ideas2it.dao.AddressDAO#deleteAddressById(String).
     */
    @Override
    public int deleteAddressById(String addressId) throws 
                                                      ApplicationException {
        String query = String.format(Constants.ADDR_DEL_QRY, addressId);
        return updateByQuery(query);
    }

	/**
     * Executing the given query(which contains the given data).
     *
     * @param query - which is to be executed.
     *
     * @return the number of objects affected
     */
    private int updateByQuery(String query) {
        Statement statement = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY + query, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    public boolean isHolderIdExists(String id) throws ApplicationException {
        Statement statement = null;
        try {
            connection = ConnectionFactory.getConnection();
            CallableStatement cStmt = connection.prepareCall("{? = call isIdExists(?)}");
            cStmt.registerOutParameter(1,java.sql.Types.BOOLEAN);
            cStmt.setString(2, id);
            cStmt.execute();
            System.out.println(cStmt.getBoolean(1));
            return cStmt.getBoolean(1);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.AddressDAO#readAddressById(String).
     */
    public Address readAddressById(String addressId) throws
                                                     ApplicationException {
        String query = String.format(Constants.ADDR_READ_QRY, addressId);
        ResultSet resultSet = readDataByQuery(query);
        try {
            if (resultSet.next()) {
                return createAddress(resultSet);
            }
            return new Address();
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR + addressId, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.AddressDAO#readAddressById(String).
     */
    public List<Address> readAllAddresss() throws ApplicationException {
        ResultSet resultSet = readDataByQuery(Constants.EMP_READ_ALL_QRY);
        List<Address> addresssInProject = null;
        try {
            addresssInProject = new ArrayList<Address> (getRowsCount
                                                             (resultSet));
            while(resultSet.next()) {
                addresssInProject.add(createAddress(resultSet));
            }
            return addresssInProject;
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.AddressDAO#readAddresssByProjectId(String).
     */
    @Override
    public List<Address> readAddresssByProjectId(String projectId) throws
                                                      ApplicationException {
        List<Address> addresssInProject = null;
        String query = String.format(Constants.EMP_READ_BY_PROJ_QRY, projectId);
        try {
            ResultSet resultSet = readDataByQuery(query);
            addresssInProject = new ArrayList<Address> (getRowsCount
                                                             (resultSet));
            while(resultSet.next()) {
                addresssInProject.add(createAddress(resultSet));
            }
            return addresssInProject;
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.EMP_RD_ALL_ERR, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.AddressDAO#createAddress(ResultSet).
     */
    private Address createAddress(ResultSet resultSet) throws
                                                    SQLException {
        try {
            return (new Address(resultSet.getString(Constants.ATTRIB_ADDR_ID),
                         resultSet.getString(Constants.ATTRIB_ADDR_NAME),
                         resultSet.getString(Constants.ATTRIB_ADDR_ONE),
                         resultSet.getString(Constants.ATTRIB_ADDR_TWO),
                         resultSet.getString(Constants.ATTRIB_ADDR_CITY),
                         resultSet.getString(Constants.ATTRIB_ADDR_ZIP),
                         resultSet.getString(Constants.ATTRIB_ADDR_STATE),
                         resultSet.getString(Constants.ATTRIB_ADDR_COUNTRY),
                         resultSet.getString(Constants.ATTRIB_ADDR_LMARK)));
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY, e);
            throw new ApplicationException(e);
        }
    }

	/**
     * Executing the given query(which contains the given data).
     *
     * @param query - which is to be executed.
     *
     * @return the read data object.
     */
    private ResultSet readDataByQuery(String query) {
        Statement statement = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY + query, e);
            throw new ApplicationException(e);
        }
    }

	/**
     * Counting the number of objects.
     *
     * @param resultSet - which has n of objects.
     *
     * @return the number of objects.
     */
    private int getRowsCount(ResultSet resultSet) {
        try {
            resultSet.last();
            int count = resultSet.getRow();
            resultSet.beforeFirst();
            return count;
        } catch(SQLException e){
            ApplicationLogger.error(Constants.DEV_ERR_EXE_QUERY, e);
            throw new ApplicationException(e);
        }
    }
}
