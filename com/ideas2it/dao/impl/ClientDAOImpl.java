package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.ConnectionFactory;
import com.ideas2it.dao.ClientDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a data access object for the entity client to do CRUD 
 * operation in database. Such as inserting details of an client into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 27/07/2017.
 */
public class ClientDAOImpl implements ClientDAO {

    /**
     * @see com.ideas2it.dao.clientDAO#insertclient(client).
     */
    @Override
    public void insertClient(Client client) throws ApplicationException {
        Connection connection =  ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.CLIENT_INS_QRY,
                 client.getId(), client.getName(), client.getEmailId());
        try {
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR + query, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.clientDAO#readclientById(String).
     */
    @Override
    public Client readClientById(String clientId) throws
        ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.CLIENT_READ_QRY, clientId);
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                return (new Client(resultSet.getString
                                            (Constants.ATTRIB_CLIENT_ID),
                                            resultSet.getString(Constants.
                                            ATTRIB_CLIENT_NAME), resultSet.
                                            getString(Constants.
                                            ATTRIB_CLIENT_EMAIL)));
            } 
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.CLIENT_RD_ERR + clientId, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * @see com.ideas2it.dao.clientDAO#readAllclient().
     */
    @Override
    public List<Client> readAllClient() throws ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        List<Client> clients = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(Constants.
                                                            CLIENT_READ_ALL_QRY);
            resultSet.last();
            clients = new ArrayList<Client>(resultSet.getRow());
            resultSet.beforeFirst();
            while(resultSet.next()) {
                    clients.add(new Client(resultSet.getString
                                            (Constants.ATTRIB_CLIENT_ID),
                                            resultSet.getString(Constants.
                                            ATTRIB_CLIENT_NAME), resultSet.
                                            getString(Constants.
                                            ATTRIB_CLIENT_EMAIL)));
            }
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.CLIENT_RD_ALL_ERR, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);

        }
        return clients;
    }

    /**
     * @see com.ideas2it.dao.clientDAO#updateclient(String, String, String).
     */
    @Override
    public int updateClient(String clientId, String attribute,
                                String data) throws ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.CLIENT_UPD_QRY, attribute,
                                        data, clientId);
        try {
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(String.format(Constants.CLIENT_UPD_ERR,
                                       clientId, attribute, data), e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.clientDAO#readclientIdByName(String).
     */
    @Override
    public String readClientIdByName(String name) throws
        ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.CLIENT_ID_QRY, name);
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
            System.out.println(resultSet.getString(Constants.ATTRIB_CLIENT_ID));
                return resultSet.getString(Constants.ATTRIB_CLIENT_ID);
            }
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.CLIENT_RD_ID_ERR + name, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * @see com.ideas2it.dao.clientDAO#deleteclientById(String).
     */
    @Override
    public int deleteClientById(String clientId) throws
        ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.CLIENT_DEL_QRY, clientId);
        try {
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.CLIENT_DEL_ERR + clientId, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
