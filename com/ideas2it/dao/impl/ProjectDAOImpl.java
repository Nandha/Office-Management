package com.ideas2it.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.ConnectionFactory;
import com.ideas2it.dao.ProjectDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Client;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a data access object for the entity project to do CRUD 
 * operation in database. Such as inserting details of an project into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * 
 * Author : Nandhakrishnan.
 * Date Created : 27/07/2017.
 */
public class ProjectDAOImpl implements ProjectDAO {

    /**
     * @see com.ideas2it.dao.ProjectDAO#insertProject(Project).
     */
    @Override
    public void insertProject(Project project) throws ApplicationException {
        Connection connection =  ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.PROJ_INS_QRY,
                 project.getId(), project.getName(), project.getClient().getId());
        try {
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.PROJ_INS_ERR + query, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#readProjectById(String).
     */
    @Override
    public Project readProjectById(String projectId) throws
        ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.PROJ_READ_QRY, projectId);
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                return (new Project(resultSet.getString
                                       (Constants.ATTRIB_PROJ_ID),
                                       resultSet.getString(Constants.
                                       ATTRIB_PROJ_NAME), new Client(resultSet.
                                       getString(Constants.ATTRIB_CLIENT_ID),
                                                    null, null)));
            } 
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ERR + projectId, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#readAllProject().
     */
    @Override
    public List<Project> readAllProject() throws ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        List<Project> projects = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(Constants.
                                                            PROJ_READ_ALL_QRY);
            resultSet.last();
            projects = new ArrayList<Project>(resultSet.getRow());
            resultSet.beforeFirst();
            while(resultSet.next()) {
                    projects.add(new Project(resultSet.getString
                                        (Constants.ATTRIB_PROJ_ID),
                                        resultSet.getString(Constants.
                                        ATTRIB_PROJ_NAME), new Client(resultSet.
                                        getString(Constants.ATTRIB_CLIENT_ID),
                                                    null, null)));
            }
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ALL_ERR, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);

        }
        return projects;
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#updateProject(String, String, String).
     */
    @Override
    public int updateProject(String projectId, String attribute,
                                String data) throws ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.PROJ_UPD_QRY, attribute,
                                        data, projectId);
        try {
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(String.format(Constants.PROJ_UPD_ERR,
                                       projectId, attribute, data), e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#readProjectIdByName(String).
     */
    @Override
    public String readProjectIdByName(String name) throws
        ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.PROJ_ID_QRY, name);
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
            System.out.println(resultSet.getString(Constants.ATTRIB_PROJ_ID));
                return resultSet.getString(Constants.ATTRIB_PROJ_ID);
            }
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ID_ERR + name, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#deleteProjectById(String).
     */
    @Override
    public int deleteProjectById(String projectId) throws
        ApplicationException {
        Connection connection = ConnectionFactory.getConnection();
        Statement statement = null;
        String query = String.format(Constants.PROJ_DEL_QRY, projectId);
        try {
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch(SQLException e) {
            ApplicationLogger.error(Constants.PROJ_DEL_ERR + projectId, e);
            throw new ApplicationException(e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
