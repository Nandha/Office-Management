package com.ideas2it.dao;

import java.util.List;
import java.sql.ResultSet;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Address;

/**
 * <p>
 * Data access object for address entity.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 22/07/2017.
 */
public interface AddressDAO {

    /**
     * <p>
     * Inserting Address details into the address table.
     * </p>
     *
     * @param address - which contains the details of an address.
     *
     * @return the number of addresss inserted.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            an address detail.
     */
    public int insertAddress(Address address) throws ApplicationException;

	/**
     * <p>
     * Reading address details from the address table by the given id.
     * </p>
     * 
     * @param addressId - by which the details of an address is read from the 
     *                     table.
     *
     * @return address which is of the given emailId.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            an address detail by the given address id.
     */
	public Address readAddressById(String addressId) throws
        ApplicationException;

	/**
     * <p>
     * Reading address's by the given project id.
     * </p>
     * 
     * @param projectId - by which the details of an address is read.
     *
     * @return addresss which has the given project id.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            all the addresss in a project by the given project id.
     */
    public List<Address> readAddresssByProjectId(String projectId) throws
        ApplicationException;

	/**
     * <p>
     * Reading all the address's.
     * </p>
     *
     * @return addresss which has the given project id.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            all the addresss in a project by the given project id.
     */
    public List<Address> readAllAddresss() throws ApplicationException;

    /**
     * <p>
     * Updating address details in the address table by the given information.
     * </p>
     *
     * @param projectId - which specifies the address to be updated.
     * @param attribute - which specifies the attribute of address to be
     *                    updated.
     * @param data - which is updated in the specified attribute of the address
     *
     * @return the number of rows updated in the address table.
     *
     * @exception ApplicationException - if a SQL error occurs, when deleting
     *            a address by the given address id.
     */
    public int deleteAddressById(String addressId) throws
        ApplicationException;

    public boolean isHolderIdExists(String id) throws ApplicationException;

	/**
     * <p>
     * Deleting an address from the address table by the given id.
     * </p>
     * 
     * @param addressId - by which an address is deleted from the table.
     *
     * @return the number of rows deleted in the address table.
     *
     * @exception ApplicationException - if a SQL error occurs, when updating
     *            an address specified by the address id by the given data.
     */
    public int updateAddress(String addressId, String attribute, String data)
        throws ApplicationException;
}
