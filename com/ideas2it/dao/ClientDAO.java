package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Client;

/**
 * <p>
 * Data access object for client entity.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 02/08/2017.
 */
public interface ClientDAO {
    /**
     * <p>
     * Inserting client details into the client table.
     * </p>
     *
     * @param client - which contains the details of a client.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            a client.
     */
    public void insertClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Reading client details from the client table by the given client id.
     * </p>
     *
     * @param clientId - by which the client detail is read from the table.
     *
     * @return the details of a client.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading a
     *            client detail by the given client id.
     */
    public Client readClientById(String clientId) throws
        ApplicationException;

    /**
     * <p>
     * Reading client details from the client table by the given client id.
     * </p>
     *
     * @param clientId - by which the client detail is read from the table.
     *
     * @return the details of all existing client.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading all
     *            the clients.
     */
    public List<Client> readAllClient() throws ApplicationException;

    /**
     * <p>
     * Deleting a client from the client table by the given client id.
     * </p>
     *
     * @param clientId - by which a client is deleted from the table.
     *
     * @return number of rows deleted from client table.
     *
     * @exception ApplicationException - if a SQL error occurs, when deleting a
     *            client by the given client id.
     */
    public int deleteClientById(String clientId) throws ApplicationException;

    /**
     * <p>
     * Reading client id from the client table by the given client name.
     * </p>
     *
     * @param clientId - by which the client id is read from the table.
     *
     * @return id which is of the given client name.
     *
     * @exception ApplicationException - if a SQL error occurs, when upadating a
     *            client by the given data.
     */
    public int updateClient(String clientId, String attribute, String data)
        throws ApplicationException;

    /**
     * <p>
     * Reading client id from the client table by the given client name.
     * </p>
     *
     * @param clientId - by which the client id is read from the table.
     *
     * @return id which is of the given client name.
     *
     * @exception ApplicationException when executing the query statement and 
     *            reading from result set.
     */
    public String readClientIdByName(String name) throws ApplicationException;
}
