package com.ideas2it.exception;

import com.ideas2it.model.Employee;

/**
 * <p>
 * Implementing custom exception by providing meaningfull information when an 
 * abnormal problem occurs due to User input.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 29/07/2017.
 */
public class UserInputException extends Exception {

    private String errorCode = null;
    private String errorMessage = null;
    private Object object = null;

    public UserInputException(String errCode, String errMsg, Object object ) {
        super(errMsg);
        this.errorCode = errCode;
        this.errorMessage = errMsg;
        this.object = object;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getMessage() {
        return this.errorMessage;
    }

    public Object getObject() {
        return this.object;
    }

}
