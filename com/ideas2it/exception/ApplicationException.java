package com.ideas2it.exception;

/**
 * <p>
 * Implementing custom exception by providing meaningfull information when an 
 * abnormal problem occurs due to User input.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 29/07/2017.
 */
public class ApplicationException extends RuntimeException {

    public ApplicationException(String errMsg, Throwable 
                                 cause) {
        super(errMsg, cause);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public ApplicationException(String errMsg) {
        super(errMsg);
    }

    public ApplicationException() {
        super();
    }
}
