package com.ideas2it.model;

/**
 * <p>
 * Employee entity with getter and setter methods.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public class Employee {
    private String id = null;
    private int age = 0;
    private String salary = null;
    private String name = null;
    private String emailId = null;
    private String dateOfBirth = null;
    private Project project = null;

    public Employee() {}

    public Employee(String id, String name, String emailId,
                       String dateOfBirth, int age, String salary,
                        Project project) {
        this.id = id;
        this.name = name;
        this.emailId = emailId;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.salary = salary;
        this.project = project;
    }

    /**
     * <p>
     * Getter and Setter methods.
     * </p>
     */
    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    public String getSalary() {
        return this.salary;  
    }

    public int getAge() {
        return this.age;  
    }

    public Project getProject() {
        return this.project;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}


