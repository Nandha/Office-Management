package com.ideas2it.model;

/**
 * <p>
 * Client entity with getter and setter methods.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public class Client {
    private String id = null;
    private String name = null;
    private String emailId = null;

    public Client() {}

    public Client(String id, String name, String emailId) {
        this.id = id;
        this.name = name;
        this.emailId = emailId;
    }

    /**
     * <p>
     * Getter and Setter methods.
     * </p>
     */
    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

}


