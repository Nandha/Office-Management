package com.ideas2it.model;

/**
 * <p>
 * Employee entity with getter and setter methods.
 * </p>
 *
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public class Address {
    private String addressHolderId = null;
    private String name = null;
    private String addressOne = null;
    private String addressTwo = null;
    private String landMark = null;
    private String city = null;
    private String zipcode = null;
    private String state = null;
    private String country = null;

    public Address() {}

    public Address(String id, String name, String addressOne,
                       String addressTwo,String city, String zipcode,
                       String state, String country, String landMark) {
        this.addressHolderId = id;
        this.name = name;
        this.addressOne = addressOne;
        this.addressTwo = addressTwo;
        this.landMark = landMark;
        this.city = city;
        this.zipcode = zipcode;
        this.state = state;
        this.country = country;
    }

    /**
     * <p>
     * Getter and Setter methods.
     * </p>
     */
    public String getHolderId() {
        return this.addressHolderId;
    }

    public String getName() {
        return this.name;
    }

    public String getAddressOne() {
        return this.addressOne;
    }

    public String getAddressTwo() {
        return this.addressTwo;
    }

    public String getLandMark() {
        return this.landMark;  
    }

    public String getCity() {
        return this.city;  
    }

    public String getZipcode() {
        return this.zipcode;
    }

    public String getState() {
        return this.state;
    }

    public String getCountry() {
        return this.country;
    }

    public void setHolderId(String id) {
        this.addressHolderId = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}


