package com.ideas2it.model;

/**
 * Project entity with getter and setter methods.
 * 
 * Author : Nandhakrishnan.
 * Date Created : 20/07/2017.
 */
public class Project {
    private String id = null;
    private String name = null;
    private Client client = null;

    public Project() {}

    public Project(String id, String name, Client client) {
        this.id = id;
        this.name = name;
        this.client = client;
    }

    /**
     * Getter and Setter methods.
     */
    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Client getClient() {
        return this.client;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}


